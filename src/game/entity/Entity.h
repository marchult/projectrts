#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "../../pathfinding/PathfindingObject.h"
#include "../../rendering/drawable/SfmlDrawable.h"
#include "../../rendering/drawable/SfmlLine.h"

class Entity : public PathfindingObject, public SFML_Drawable
{
public:
	Entity(bool canMove = true);
	~Entity();

	void SetPosition(const sf::Vector2f& position) override;
	void SetPosition(const Float2& position) override;
	void SetPosition(float x, float y)  override;
	void SetSize(const sf::Vector2f& size) override;
	void SetSize(const Float2& size) override;
	void SetSize(float x, float y)  override;
	void SetRotation(float r)  override;
	
	void Move(const sf::Vector2f& direction)  override;
	void Move(const Float2& direction);
	void Move(float x, float y)  override;
	void Scale(const sf::Vector2f& scale)  override;
	void Scale(const Float2& scale);
	void Scale(float x, float y)  override;
	void Rotate(float r)  override;

	void Update(float dt);
	void DrawPathingLines();
	void SetSpeed(float speed) { m_speed = speed; }
	float GetSpeed() { return m_speed; }

	void SetTeam(int team) { m_team = team; }
	int GetTeam() const { return m_team; }

	void SetViewDistance(float dist) { m_viewDistance = dist; }
	float GetViewDistance() const { return m_viewDistance; }

private:
	void _updatePositions(float dt);
	void _pathing(float dt);

private:
	float m_health = 100;
	float m_speed = 10;
	std::vector<Float2> m_currentPath;

	std::vector<SFML_Line> m_pathLines;
	float m_distanceToGoal;
	int m_team = -1;
	float m_viewDistance = 50;
};

#endif
