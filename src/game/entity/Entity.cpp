#include "Entity.h"

Entity::Entity(bool canMove) : PathfindingObject(), SFML_Drawable()
{
	PathfindingObject::SetAABBHitbox({ 32.0f, 32.0f });
	SFML_Drawable::SetSize(32, 32);
	m_distanceToGoal = FLT_MAX;
}

Entity::~Entity()
{

}

void Entity::SetPosition(const sf::Vector2f& position)
{
	SFML_Drawable::SetPosition(position);
	PathfindingObject::SetPosition({ position.x, position.y });
}

void Entity::SetPosition(const Float2& position)
{
	SFML_Drawable::SetPosition({ position.x, position.y });
	PathfindingObject::SetPosition(position);
}

void Entity::SetPosition(float x, float y)
{
	SFML_Drawable::SetPosition({ x, y });
	PathfindingObject::SetPosition({ x, y });
}

void Entity::SetSize(const sf::Vector2f& size)
{
	SFML_Drawable::SetSize(size);
	PathfindingObject::SetSize({ size.x * 0.5f, size.y * 0.5f });
}

void Entity::SetSize(const Float2& size)
{
	SFML_Drawable::SetSize({ size.x, size.y });
	PathfindingObject::SetSize(size * 0.5f);
}

void Entity::SetSize(float x, float y)
{
	SFML_Drawable::SetSize({ x, y });
	PathfindingObject::SetSize({ x * 0.5f, y * 0.5f });
}

void Entity::SetRotation(float r)
{
	SFML_Drawable::SetRotation(r);
	//PathfindingObject::SetRotation(Convert::DegreeseToRadians(r));
}

void Entity::Move(const sf::Vector2f& direction)
{
	SFML_Drawable::Move(direction);
	sf::Vector2f newPos = SFML_Drawable::GetPosition();
	PathfindingObject::SetPosition({ newPos.x, newPos.y });
}

void Entity::Move(const Float2& direction)
{
	SFML_Drawable::Move(Convert::ConvertToSfmlVec(direction));
	sf::Vector2f newPos = SFML_Drawable::GetPosition();
	PathfindingObject::SetPosition({ newPos.x, newPos.y });
}

void Entity::Move(float x, float y)
{
	SFML_Drawable::Move(x, y);
	sf::Vector2f newPos = SFML_Drawable::GetPosition();
	PathfindingObject::SetPosition({ newPos.x, newPos.y });
}

void Entity::Scale(const sf::Vector2f& scale)
{
	SFML_Drawable::Scale(scale);
	sf::Vector2f newScl = SFML_Drawable::GetSize();
	PathfindingObject::SetSize({ newScl.x * 0.5f, newScl.y * 0.5f});
}

void Entity::Scale(const Float2& scale)
{
	SFML_Drawable::Scale(Convert::ConvertToSfmlVec(scale));
	sf::Vector2f newScl = SFML_Drawable::GetSize();
	PathfindingObject::SetSize({ newScl.x * 0.5f, newScl.y * 0.5f });
}

void Entity::Scale(float x, float y)
{
	SFML_Drawable::Scale(x, y);
	sf::Vector2f newScl = SFML_Drawable::GetSize();
	PathfindingObject::SetSize({ newScl.x * 0.5f, newScl.y * 0.5f });
}

void Entity::Rotate(float r)
{
	SFML_Drawable::Rotate(r);
	float newR = SFML_Drawable::GetRotation();
	PathfindingObject::SetRotation(Convert::DegreeseToRadians(newR));
}

void Entity::Update(float dt)
{
	_updatePositions(dt);
	_pathing(dt);
}

void Entity::DrawPathingLines()
{
	for (auto& l : m_pathLines)
		l.Draw();
}

void Entity::_updatePositions(float dt)
{
	UpdateFromPhysics();

	sf::Vector2f newPos = Convert::ConvertToSfmlVec(PathfindingObject::GetHitboxPosition());
	float rotation = Convert::RadiansToDegreese(PathfindingObject::GetHitboxRotation());
	SFML_Drawable::SetPosition(newPos);
	//SFML_Drawable::SetRotation(rotation);
}

void Entity::_pathing(float dt)
{
	//std::unique_lock<std::mutex> lock(m_pathMutex);
	if (m_isNewPathAvailable || m_tempPath)
	{
		m_currentPath = m_path;
		m_isRequestingPath = false;
		m_distanceToGoal = FLT_MAX;
		m_pathLines.clear();
		if (!m_currentPath.empty())
		{
			size_t size = m_currentPath.size();
			size_t closest = 0;
			float dist = FLT_MAX;
			float tDist;

			for (size_t i = 0; i < size; i++)
			{
				tDist = (m_currentPath[i] - GetHitboxPosition()).lengthSq();
				if (tDist < dist)
				{
					dist = tDist;
					closest = i;
				}
			}

			if (closest != 0)
				m_currentPath.erase(m_currentPath.begin(), m_currentPath.begin() + (closest - 1));

			m_pathLines.resize(m_currentPath.size());
			m_pathLines[0].SetPosition(GetPosition(), { m_currentPath[0].x, m_currentPath[0].y });

			for (int i = 1; i < m_pathLines.size(); i++)
			{
				m_pathLines[i].SetPosition(m_pathLines[i - 1].GetP2(), { m_currentPath[i].x, m_currentPath[i].y });
			}
		}

		if (m_isNewPathAvailable)
			m_isNewPathAvailable = false;
		if (m_tempPath)
			m_tempPath = false;
	}

	if (!m_currentPath.empty() && !m_unableToMove)
	{
		Float2 towardsGoal = (m_currentPath[0] - Convert::ConvertToFloat2(GetPosition()));
		float l = towardsGoal.lengthSq();

		if (!m_pathLines.empty())
		{
			m_pathLines[0].SetColor(sf::Color::Magenta);
			m_pathLines[0].SetPosition1(GetPosition());
		}

		if (l < GetHitboxSize().lengthSq())
		{
			//SetPosition(m_currentPath[0].x, m_currentPath[0].y);
			m_currentPath.erase(m_currentPath.begin());
			m_pathLines.erase(m_pathLines.begin());
			m_distanceToGoal = FLT_MAX;
		}
		else
		{
			m_distanceToGoal = l;

			Float2 lv = GetLinearVelocity();
			lv = lv + towardsGoal.normalize() * m_speed;

			Float2 finalDir = lv.normalize();
			if (lv.lengthSq() > m_speed* m_speed)
			{
				lv = finalDir * m_speed;
			}

			Float2 up(0, -1);

			float deg = Convert::RadiansToDegreese(up.Angle(finalDir));

			PathfindingObject::SetLinearVelocity(lv.Cap(-m_speed, m_speed));

			SetRotation(deg);
		}
	}
}
