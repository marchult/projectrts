#ifndef _STRUCTS_H_
#define _STRUCTS_H_
#include <cmath>
struct Float2
{
	Float2(float _x = 0.0f, float _y = 0.0f)
	{
		x = _x;
		y = _y;
	}

	float lengthSq() const
	{
		return x * x + y * y;
	}
	float length() const
	{
		float lsq = lengthSq();
		return sqrt(lsq);
	}
	Float2 normalize() const
	{
		return *this / length();
	}

	Float2 operator+(const Float2& other) const
	{
		return Float2(x + other.x, y + other.y);
	}
	Float2 operator-(const Float2& other) const
	{
		return Float2(x - other.x, y - other.y);
	}
	Float2 operator*(const Float2& other) const
	{
		return Float2(x * other.x, y * other.y);
	}
	Float2 operator*(float t) const
	{
		return Float2(x * t, y * t);
	}
	Float2 operator/(const Float2& other) const
	{
		return Float2(x / other.x, y / other.y);
	}
	Float2 operator/(float t) const
	{
		return Float2(x / t, y / t);
	}

	Float2 abs()
	{
		return Float2(std::abs(x), std::abs(y));
	}

	Float2 Cap(float _min, float _max)
	{
		Float2 f;
		
		f.x = x < _min ? _min : x;
		f.x = x > _max ? _max : x;

		f.y = y < _min ? _min : y;
		f.y = y > _max ? _max : y;

		return f;
	}

	Float2 Cap(const Float2 & _min, const Float2 & _max)
	{
		Float2 f;

		f.x = x < _min.x ? _min.x : x;
		f.x = x > _max.x ? _max.x : x;

		f.y = y < _min.y ? _min.y : y;
		f.y = y > _max.y ? _max.y : y;

		return f;
	}

	Float2 Rotate(float angle)
	{
		Float2 v;
		float cosa = cos(angle);
		float sina = sin(angle);
		
		v.x = x * cosa + y * sina;
		v.y = -x * sina + y * cosa;

		return v;
	}

	float Dot(const Float2& f)
	{
		return x * f.x + y * f.y;
	}

	float Det(const Float2& f)
	{
		return x * f.y - y * f.x;
	}

	float Angle(const Float2& f)
	{
		return atan2f(Det(f), Dot(f));
	}

	float x;
	float y;
};

struct Float3
{
	Float3(float _x = 0.0f, float _y = 0.0f, float _z = 0.0f)
	{
		x = _x;
		y = _y;
		z = _z;
	}

	float lengthSq() const
	{
		return x * x + y * y + z * z;
	}
	float length() const
	{
		float lsq = lengthSq();
		return sqrt(lsq);
	}
	Float3 normalize() const
	{
		return *this / length();
	}

	Float3 operator+(const Float3& other) const
	{
		return Float3(x + other.x, y + other.y, z + other.z);
	}
	Float3 operator-(const Float3& other) const
	{
		return Float3(x - other.x, y - other.y, z - other.z);
	}
	Float3 operator*(const Float3& other) const
	{
		return Float3(x * other.x, y * other.y, z * other.z);
	}
	Float3 operator*(float t) const
	{
		return Float3(x * t, y * t, z * t);
	}
	Float3 operator/(const Float3& other) const
	{
		return Float3(x / other.x, y / other.y, z / other.z);
	}
	Float3 operator/(float t) const
	{
		return Float3(x / t, y / t, z / t);
	}

	Float3 abs()
	{
		return Float3(std::abs(x), std::abs(y), std::abs(z));
	}

	Float3 Cap(float _min, float _max)
	{
		Float3 f;

		f.x = x < _min ? _min : x;
		f.x = x > _max ? _max : x;

		f.y = y < _min ? _min : y;
		f.y = y > _max ? _max : y;

		f.z = z < _min ? _min : z;
		f.z = z > _max ? _max : z;

		return f;
	}

	Float3 Cap(const Float3& _min, const Float3& _max)
	{
		Float3 f;

		f.x = x < _min.x ? _min.x : x;
		f.x = x > _max.x ? _max.x : x;

		f.y = y < _min.y ? _min.y : y;
		f.y = y > _max.y ? _max.y : y;

		f.z = z < _min.z ? _min.z : z;
		f.z = z > _max.z ? _max.z : z;

		return f;
	}

	Float3 Rotate(float x, float y, float z)
	{
		return Float3();
	}

	float Dot(const Float3& f)
	{
		return x * f.x + y * f.y + z * f.z;
	}

	Float3 Cross(const Float3& v)
	{
		Float3 c;
		
		c.x = y * v.z - z * v.y;
		c.y = x * v.z - z * v.x;
		c.z = x * v.y - y * v.x;

		return c;
	}

	float x;
	float y;
	float z;
};

struct Uint2
{
	unsigned int x;
	unsigned int y;
};

#endif // !_STRUCTS_H_

