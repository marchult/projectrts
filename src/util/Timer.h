#ifndef _TIMER_H_
#define _TIMER_H_

#include <SFML/System/Clock.hpp>

class Timer
{
public:
	void Start()
	{		
		m_clock.restart();
	}
	float Restart()
	{
		return m_clock.restart().asSeconds();
	}
	float GetElapsedTime()
	{
		return m_clock.getElapsedTime().asSeconds();
	}
private:
	sf::Clock m_clock;
};

#endif 
