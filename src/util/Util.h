#ifndef _UTIL_H_
#define _UTIL_H_

#include <SFML/System/Vector2.hpp>
#include <Box2D/Common/b2Math.h>
#include "Structs.h"

namespace Constants
{
	static const double PI = 3.14159265359;
}

class Convert
{
public:
	static double RadiansToDegreese(double rad)
	{
		return rad * (180.0 / Constants::PI);
	}
	static double DegreeseToRadians(double deg)
	{
		return deg * (Constants::PI / 180.0);
	}

	static Float2 ConvertToFloat2(const sf::Vector2f& vec)
	{
		return Float2(vec.x, vec.y);
	}
	static Float2 ConvertToFloat2(const b2Vec2& vec)
	{
		return Float2(vec.x, vec.y);
	}
	static sf::Vector2f ConvertToSfmlVec(const Float2& vec)
	{
		return sf::Vector2f(vec.x, vec.y);
	}
	static sf::Vector2f ConvertToSfmlVec(const b2Vec2& vec)
	{
		return sf::Vector2f(vec.x, vec.y);
	}
	static b2Vec2 ConvertToB2Vec(const Float2& vec)
	{
		return b2Vec2(vec.x, vec.y);
	}
	static b2Vec2 ConvertToB2Vec(const sf::Vector2f& vec)
	{
		return b2Vec2(vec.x, vec.y);
	}
};

#endif