#ifndef _PATHFINDING_H_
#define _PATHFINDING_H_

#include <iostream>
#include <vector>
#include <map>
#include "../util/Structs.h"
#include <imgui.h>

#include "../rendering/drawable/SfmlDrawable.h"

#include <queue>
#include <mutex>
#include <thread>
#include <functional>

class PathfindingObject;
class World;

class Pathfinding
{
public:
	enum COL_TYPE
	{
		STATIC_TYPE,
		DYNAMIC_TYPE,
		ALL
	};
private:
	static bool STATIC;
	static bool DYNAMIC;
	struct Tile
	{
	public:
		Tile(const Float2& pos = { 0.0f, 0.0f }, const Float2& size = { 0.0f, 0.0f }, unsigned int depth = 0, bool createChildren = false);
		
		void GetNeighbour(Tile* t, std::vector<Tile*> & vector);
		Tile* GetTile(const Float2& point);
		Tile* GetTile3(const Float2& point, const Float2& size);

		void GetNeighbour2(Tile* t, std::vector<Tile*>& vector, PathfindingObject * po);
		Tile* GetTile2(const Float2& point, PathfindingObject* po, PathfindingObject* ignoreCol);

		void Clean();
		void Clear();
		void Place(PathfindingObject* object);
		bool Inside(const Float2& point);

		PathfindingObject* ExistCollisionFor(PathfindingObject* object, COL_TYPE type);
		void GetAllObjectsCollidingWith(PathfindingObject* object, std::vector<PathfindingObject*> & vector, COL_TYPE type);
		bool IsLeaf();
		bool IsEmpty();

	public:
		SFML_Drawable D;

		void Draw();

		static unsigned int Max_Depth;
		Float2 Position, Size;
		unsigned int Depth;
		Tile* pChildren[4] = {nullptr, nullptr, nullptr, nullptr};
		Tile* pParent = nullptr;
		bool HasChildren = false;
		bool AllocatedChildren = false;

		// PUT THESE VECTORS IN A MAP AND THE KEY IS BOOL, ONE FOR STATIC AND ONE FOR DYNAMIC
		std::map<bool,std::vector<PathfindingObject*>> ObjectVector;
		std::map<bool, std::vector<PathfindingObject*>> FamilyObjectVector;
	};

	struct Node
	{
		Node()
		{
			fCost = FLT_MAX;
			gCost = FLT_MAX;
			hCost = FLT_MAX;
			current = nullptr;
			parentNodeIndex = -1;
		}
		Node(int _parentNodeIndex, Tile* _current, float _gCost, float _hCost, const Float2 & _worldPosition)
		{
			current = _current;
			parentNodeIndex = _parentNodeIndex;
			fCost = _gCost + _hCost;
			gCost = _gCost;
			hCost = _hCost;
			worldPosition = _worldPosition;
		}

		bool operator<(const Node& other) const
		{
			return fCost < other.fCost;
		}

		Float2 worldPosition;

		Tile* current = nullptr;
		int parentNodeIndex = -1;

		float fCost = FLT_MAX, gCost = FLT_MAX, hCost = FLT_MAX;
	};

public:
	static unsigned int DRAW_DEPTH;
	static unsigned int MAX_COMPARE_ALLOWED;

	static void Initialize(const Float2& worldSize, const Float2& startPosition = { 0.0f, 0.0f }, unsigned int maxDepth = 7, unsigned int nrofThreads = 4);
	static void Release();
	static Pathfinding* GetInstance();
	std::vector<Float2> GetSingleAgentPath(PathfindingObject* po, const Float2 & from, const Float2 & to);
	std::vector<Float2> GetFinnishedPathFor(PathfindingObject* po);
	void RequestSingleAgentPath(PathfindingObject* po, const Float2 & from, const Float2 & to);
	bool IsPathReady(PathfindingObject* po);
	void Rebuild(const std::vector<PathfindingObject*> & objects);
	void Place(PathfindingObject* pObj);
	void Clean();
	void Clear();
	// Returns the first pathfinding object intersecting with object
	PathfindingObject* ExistCollisionFor(PathfindingObject* pObj, COL_TYPE type = ALL);

	const Float2& GetWorldStart() const;
	const Float2& GetWorldEnd() const;

	// Returns all pathfinding objects intersecting with pObj
	std::vector<PathfindingObject*> GetAllObjectsCollidingWith(PathfindingObject* pObj, COL_TYPE type = ALL);
	
	void TestDrawNeighbours(PathfindingObject* p);

	//DEBUG
	void Draw();
private:
	Pathfinding(const Float2 & pos, const Float2 & size);
	~Pathfinding();

	void _build(const std::vector<PathfindingObject*>& objects);

	std::vector<Tile*> _getNeighbours(Tile* t);
	Tile* _getTile(const Float2& point);

	std::vector<Tile*> _getNeighbours2(Tile* t, PathfindingObject * po);
	Tile* _getTile2(const Float2& point, PathfindingObject * po, PathfindingObject * ignoreCollision = nullptr);

	Tile* _getTile3(const Float2& point, const Float2& size);

	float _calcHValue(const Float2 & s, const Float2 & d);

private:
	static bool		s_isInitialized;
	static Pathfinding* s_singleton;

	Tile* m_rootTile;

// Thread pool
private:
	class WorkerThread
	{
	public:
		std::thread Worker;
		PathfindingObject* Id;
		bool IsDone;

		void Update()
		{
			if (Worker.get_id() == std::thread::id())
			{
				Worker = std::thread(std::bind(&Pathfinding::_waitingForWork, Pathfinding::GetInstance()));
				std::cout << "Thread Restarted" << std::endl;
			}
		}

		WorkerThread(std::function<void()> func)
		{
			Worker = std::thread(func);
			Id = nullptr;
			IsDone = false;
		}
		~WorkerThread()
		{
			Worker.join();
		}
	};

	struct ThreadArguments
	{
		Float2 S;
		Float2 D;
	};

	void _waitingForWork();

	unsigned int m_threadCount;
	bool m_isRunning;
	std::map<std::thread::id, WorkerThread*> m_pool;
	std::queue<std::pair<ThreadArguments, PathfindingObject*>> m_work;
	std::mutex m_workMutex;
	std::mutex m_workIdMutex;
	std::mutex m_treeMutex;
	std::condition_variable m_condition;
	std::map<PathfindingObject*, std::vector<Float2>> m_results;

	static std::vector<Float2> _findPath(
		PathfindingObject* key,
		const Float2& from,
		const Float2& to
	);
};
#endif
