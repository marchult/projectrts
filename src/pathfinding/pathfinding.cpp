#include "pathfinding.h"
#include "PathfindingObject.h"
#include "World.h"
#include <iostream>
#include "World.h"

bool			Pathfinding::s_isInitialized = false;
Pathfinding*	Pathfinding::s_singleton = nullptr;
unsigned int	Pathfinding::Tile::Max_Depth = 1;
unsigned int	Pathfinding::DRAW_DEPTH = 0;
unsigned int	Pathfinding::MAX_COMPARE_ALLOWED = 8;
bool			Pathfinding::DYNAMIC = false;
bool			Pathfinding::STATIC = true;


void Pathfinding::Initialize(const Float2& worldSize, const Float2& startPosition, unsigned int maxDepth, unsigned int nrofThreads)
{

	if (!s_isInitialized)
	{
		World::WORLD_SIZE_X = worldSize.x;
		World::WORLD_SIZE_Y = worldSize.y;

		Pathfinding::Tile::Max_Depth = maxDepth;
		s_isInitialized = true;
		s_singleton = new Pathfinding(startPosition, worldSize);
		s_singleton->m_threadCount = nrofThreads;
		s_singleton->m_isRunning = true;

		for (unsigned int i = 0; i < nrofThreads; i++)
		{
			std::function<void()> func = std::bind(&Pathfinding::_waitingForWork, s_singleton);
			WorkerThread* wt = new WorkerThread(func);
			s_singleton->m_pool.insert(std::make_pair(wt->Worker.get_id(), wt));
		}

	}
}

void Pathfinding::Release()
{
	if (s_isInitialized)
	{
		s_isInitialized = false;
		
		s_singleton->Clean();
		s_singleton->m_isRunning = false;
		s_singleton->m_condition.notify_all();
		for (auto& t : s_singleton->m_pool)
			delete t.second;
		s_singleton->m_pool.clear();

		for (auto& t : s_singleton->m_results)
			t.second.clear();
		s_singleton->m_results.clear();

		delete s_singleton;
		s_singleton = nullptr;
	}
}

Pathfinding* Pathfinding::GetInstance()
{
	return s_singleton;
}

std::vector<Float2> Pathfinding::GetSingleAgentPath(PathfindingObject* po, const Float2& from, const Float2& to)
{
	return _findPath(po, from, to);
}

std::vector<Float2> Pathfinding::GetFinnishedPathFor(PathfindingObject* po)
{
	std::unique_lock<std::mutex> lock(m_workIdMutex);
	std::vector<Float2> r = m_results[po];
	m_results.erase(po);
	return r;
}

void Pathfinding::RequestSingleAgentPath(PathfindingObject* pr, const Float2& from, const Float2& to)
{
	ThreadArguments tA;
	tA.S = from;
	tA.D = to;
	{
		std::unique_lock<std::mutex> lock(m_workMutex);
		m_work.push(std::make_pair(tA, pr));
	}
	m_condition.notify_one();
}

bool Pathfinding::IsPathReady(PathfindingObject* pr)
{
	std::unique_lock<std::mutex> lock(m_workIdMutex);
	auto it = m_results.find(pr);
	return it != m_results.end();
}

void Pathfinding::Rebuild(const std::vector<PathfindingObject*>& objects)
{
	{
		for (auto& t : m_pool)
		{
			t.second->Update();
		}

		std::unique_lock<std::mutex> lock(m_treeMutex);
		Clear();
		_build(objects);
	}
}

void Pathfinding::Place(PathfindingObject* pObj)
{
	m_rootTile->Place(pObj);
}

void Pathfinding::Clean()
{
	m_rootTile->Clean();
}

void Pathfinding::Clear()
{
	m_rootTile->Clear();
}

PathfindingObject* Pathfinding::ExistCollisionFor(PathfindingObject* pObj, Pathfinding::COL_TYPE type)
{
	return m_rootTile->ExistCollisionFor(pObj, type);
}

const Float2& Pathfinding::GetWorldStart() const
{
	return m_rootTile->Position;
}

const Float2& Pathfinding::GetWorldEnd() const
{
	return m_rootTile->Size;
}

std::vector<PathfindingObject*> Pathfinding::GetAllObjectsCollidingWith(PathfindingObject* pObj, Pathfinding::COL_TYPE type)
{
	std::vector<PathfindingObject*> ret;

	m_rootTile->GetAllObjectsCollidingWith(pObj, ret, type);

	return ret;
}

void Pathfinding::TestDrawNeighbours(PathfindingObject* p)
{
	Tile * t = _getTile2(p->GetHitboxPosition(), p);

	if (t)
	{
		auto vec = _getNeighbours2(t, p);

		for (auto& tt : vec)
		{
			tt->D.SetOutlineColor(sf::Color::Red);
		}
	}
}

void Pathfinding::Draw()
{
	m_rootTile->Draw();
}

Pathfinding::Pathfinding(const Float2& pos, const Float2& size)
{
	m_rootTile = new Tile(pos, size, 0, true);
}

Pathfinding::~Pathfinding()
{

}

void Pathfinding::_build(const std::vector<PathfindingObject*>& objects)
{
	for (auto& pObj : objects)
	{
		m_rootTile->Place(pObj);
	}
}

std::vector<Pathfinding::Tile*> Pathfinding::_getNeighbours(Tile* t)
{
	static const float _EPSILON = 0.0001f;
	std::vector<Tile*> n;
	Tile tt;

	tt.Position = t->Position;
	tt.Size = t->Size;
	tt.Position.x -= _EPSILON;
	tt.Size.x += _EPSILON * 2;
	
	std::unique_lock<std::mutex> lock(m_treeMutex);
	m_rootTile->GetNeighbour(&tt, n);

	tt.Position = t->Position;
	tt.Size = t->Size;
	tt.Size.y += _EPSILON * 2;
	tt.Position.y -= _EPSILON;

	m_rootTile->GetNeighbour(&tt, n);

	return n;
}

Pathfinding::Tile* Pathfinding::_getTile(const Float2& point)
{
	std::unique_lock<std::mutex> lock(m_treeMutex);
	return m_rootTile->GetTile(point);
}

std::vector<Pathfinding::Tile*> Pathfinding::_getNeighbours2(Tile* t, PathfindingObject* po)
{
	static const float _EPSILON = 0.001f;
	std::vector<Tile*> n;
	Tile tt;

	tt.Position = t->Position;
	tt.Size = t->Size;
	tt.Position.x -= _EPSILON;
	tt.Size.x += _EPSILON * 2;
	std::unique_lock<std::mutex> lock(m_treeMutex);
	m_rootTile->GetNeighbour2(&tt, n, po);

	tt.Position = t->Position;
	tt.Size = t->Size;
	tt.Size.y += _EPSILON * 2;
	tt.Position.y -= _EPSILON;
	
	m_rootTile->GetNeighbour2(&tt, n, po);

	return n;
}

Pathfinding::Tile* Pathfinding::_getTile2(const Float2& point, PathfindingObject* po, PathfindingObject* ignoreCollision)
{
	if (!ignoreCollision)
		ignoreCollision = po;

	std::unique_lock<std::mutex> lock(m_treeMutex);
	return m_rootTile->GetTile2(point, po, ignoreCollision);
}

Pathfinding::Tile* Pathfinding::_getTile3(const Float2& point, const Float2& size)
{
	std::unique_lock<std::mutex> lock(m_treeMutex);
	return m_rootTile->GetTile3(point, size);
}

float Pathfinding::_calcHValue(const Float2& s, const Float2& d)
{

	/*
	// STANFORD_DISTANCE
	Float2 delta = (d - s).abs();
	return std::max(delta.x, delta.y) + (-0.414f) * std::min(delta.x, delta.y);
	*/

	return (d - s).length();
}

void Pathfinding::_waitingForWork()
{
	std::function<std::vector<Float2>(PathfindingObject*, const Float2&, const Float2&)> work(&Pathfinding::_findPath);
	ThreadArguments tA;
	PathfindingObject* id;


	while (m_isRunning)
	{
		{
			std::unique_lock<std::mutex> lock(m_workMutex);
			m_condition.wait(lock, [this] {return !m_work.empty() || !m_isRunning; });
			if (!m_isRunning)
				return;
			m_pool[std::this_thread::get_id()]->IsDone = false;
			tA = m_work.front().first;
			id = m_work.front().second;
			m_work.pop();
		}

		std::vector<Float2> returnValue = work(id, tA.S, tA.D);
		m_pool[std::this_thread::get_id()]->IsDone = true;

		//std::unique_lock<std::mutex> lock(m_workIdMutex);
		id->SetPath(returnValue);
		//m_results.insert(std::make_pair(id, returnValue));
		//m_results.insert_or_assign(id, returnValue);
	}

}

#include <windows.h>

std::vector<Float2> Pathfinding::_findPath(PathfindingObject* po, const Float2& from, const Float2& to)
{
	if (po->GetPathingType() == PathfindingObject::FLYING)
	{
		std::vector<Float2> p(2);
		p[0] = from;
		p[1] = to;
		return p;
	}

	World* world = World::GetInstance();
	Float2 goal = to;


	float planB_closest_dist = FLT_MAX;
	size_t planB_nodeIndex = 0;

	if (to.x < s_singleton->m_rootTile->Position.x || to.y < s_singleton->m_rootTile->Position.y ||
		to.x > s_singleton->m_rootTile->Position.x + s_singleton->m_rootTile->Size.x ||
		to.y > s_singleton->m_rootTile->Position.y + s_singleton->m_rootTile->Size.y)
	{
		Float2 closestPoint;
		closestPoint.x = std::max(s_singleton->m_rootTile->Position.x, std::min(to.x, s_singleton->m_rootTile->Position.x + s_singleton->m_rootTile->Size.x));
		closestPoint.y = std::max(s_singleton->m_rootTile->Position.y, std::min(to.y, s_singleton->m_rootTile->Position.y + s_singleton->m_rootTile->Size.y));
		goal = closestPoint;
	}

	//Tile* source = s_singleton->_getTile3(from, po->GetHitboxSize());
	Tile* source = s_singleton->_getTile2(po->GetHitboxPosition(), po);
	
	Tile* dest = nullptr;
	auto type = po->GetHitboxType();
	PathfindingObject poDest;


	switch (type)
	{
	case PathfindingObject::Circle:
		poDest = PathfindingObject::CreateCircleHitbox(po->GetHitboxSize().x, goal);
		break;
	case PathfindingObject::Rectangle:
		poDest = PathfindingObject::CreateRectangleHitbox(po->GetHitboxSize(), po->GetHitboxRotation(), goal);
		break;
	case PathfindingObject::AABB:
		poDest = PathfindingObject::CreateAABBHitbox(po->GetHitboxSize(), goal);
		break;
	}	
	dest = s_singleton->_getTile2(goal, &poDest, po);

	if (!dest)
	{
		Float2 newGoal = goal;

		auto col = s_singleton->GetAllObjectsCollidingWith(&poDest, COL_TYPE::STATIC_TYPE);
		int counter = 0;
		PathfindingObject* colP = col.front();
		Float2 closestPoint = colP->GetClosestPointFrom(newGoal);
				
		Float2 cpToGoal;

		if (colP->Contains(newGoal))
			cpToGoal = (closestPoint - newGoal).normalize();
		else
			cpToGoal = (newGoal - closestPoint).normalize();
				
		Float2 offset = poDest.GetHitboxSize() * cpToGoal * 0.1;
		newGoal = closestPoint + offset;

		dest = s_singleton->_getTile2(newGoal, &poDest, po);

		while (!dest && counter < 1000)
		{
			newGoal = newGoal + offset;
			poDest.SetPosition(newGoal);
			//dest = s_singleton->_getTile2(newGoal, &poDest, po);
			dest = s_singleton->_getTile3(newGoal, poDest.GetHitboxSize());
			counter++;
		}

		goal = newGoal;
	}

	if (!source || !dest)
		return std::vector<Float2>();

	std::map<Tile*, bool> closedList;

	std::vector<Node> openList;
	std::vector<Node> earlyExploration;
	std::vector<Node> nodes;

	Node nSource(-1, source, 0.f, s_singleton->_calcHValue(from, goal), from);

	Node currentNode = nSource;
	Node earlyExporationNode;

	openList.push_back(currentNode);

	while (!openList.empty() || earlyExporationNode.parentNodeIndex != -1)
	{
		if (earlyExporationNode.parentNodeIndex != -1)
		{
			currentNode = earlyExporationNode;
			earlyExporationNode.parentNodeIndex = -1;
		}
		else
		{
			std::sort(openList.begin(), openList.end());
			currentNode = openList.front();
			openList.erase(openList.begin());
		}
		nodes.push_back(currentNode);

		float planB_CurrDir = (nodes.back().worldPosition - goal).lengthSq();
		if (planB_CurrDir < planB_closest_dist)
		{
			planB_nodeIndex = nodes.size() - 1;
			planB_closest_dist = planB_CurrDir;
		}

		// TODO :: TEST THIS
		bool found = currentNode.current == dest;

		if (found)
		{
			RayTracerAll rta(currentNode.worldPosition);
			world->Getb2World()->RayCast(&rta, Convert::ConvertToB2Vec(currentNode.worldPosition), Convert::ConvertToB2Vec(goal));
			if (rta.HasContacts())
			{
				auto contacts = rta.GetAllContacts();
				for (auto& c : contacts)
				{
					b2EdgeShape* e = (b2EdgeShape*)c.first->GetShape();
					if (e)
					{
						found = false;
						break;
					}
				}
			}
		}

		if (found)
		{
			std::vector<Float2> finalPath;
			while (currentNode.parentNodeIndex != -1)
			{
				finalPath.push_back(currentNode.worldPosition);
				currentNode = nodes[currentNode.parentNodeIndex];
			}
			std::reverse(finalPath.begin(), finalPath.end());
			finalPath.push_back(goal);
			return finalPath;
		}

		closedList.insert_or_assign(currentNode.current, true);
		int parentIndex = (int)nodes.size() - 1;

		std::vector<Tile*> neighbours = s_singleton->_getNeighbours2(currentNode.current, po);
		
		for (auto& t : neighbours)
		{
			if (closedList.find(t) == closedList.end())
			{
				Float2 sharedPoint;
				sharedPoint.x = std::max(t->Position.x, std::min(currentNode.worldPosition.x, t->Position.x + t->Size.x));
				sharedPoint.y = std::max(t->Position.y, std::min(currentNode.worldPosition.y, t->Position.y + t->Size.y));

				

				Float2 poSize = po->GetHitboxSize();
				if (po->GetHitboxType() == PathfindingObject::Circle)
				{
					poSize.y = poSize.x;
				}

				bool rightOfCentrum = sharedPoint.x > t->Position.x + t->Size.x * 0.5f;
				bool belowOfCentrum = sharedPoint.y > t->Position.y + t->Size.y * 0.5f;

				float xPoint = t->Position.x + t->Size.x * rightOfCentrum; // One out of four corners
				float yPoint = t->Position.y + t->Size.y * belowOfCentrum;

				if (rightOfCentrum)
					sharedPoint.x += xPoint - (sharedPoint.x + poSize.x);
				else if (!rightOfCentrum)
					sharedPoint.x += poSize.x;
				if (belowOfCentrum)
					sharedPoint.y += yPoint - (sharedPoint.y + poSize.y);
				else if (!belowOfCentrum)
					sharedPoint.y += poSize.y;

				bool isOk = true;
				
				PathfindingObject::PATHING_TYPE pft = po->GetPathingType();
				
				switch (pft)
				{
				case PathfindingObject::LAND:
					isOk = world->IsType(sharedPoint.x, sharedPoint.y, Region::Land);
					break;
				case PathfindingObject::LAND_ALL:
					isOk = !world->IsType(sharedPoint.x, sharedPoint.y, Region::Water);
					break;
				case PathfindingObject::WATER:
					isOk = world->IsType(sharedPoint.x, sharedPoint.y, Region::Water);
					break;
				case PathfindingObject::WATER_AND_LAND:
					isOk = !world->IsType(sharedPoint.x, sharedPoint.y, Region::Blocked);
					break;
				}

				RayTracerAll rta(currentNode.worldPosition);
				world->Getb2World()->RayCast(&rta, Convert::ConvertToB2Vec(currentNode.worldPosition), Convert::ConvertToB2Vec(sharedPoint));
				if (rta.HasContacts())
				{
					auto contacts = rta.GetAllContacts();
					for (auto& c : contacts)
					{
						b2EdgeShape* e = (b2EdgeShape*)c.first->GetShape();
						if (e)
						{
							isOk = false;
							break;
						}
					}
				}

				if (isOk)
				{
					Float2 towards = sharedPoint - currentNode.current->Position;
					Node newNode(parentIndex, t, currentNode.gCost + towards.length(), s_singleton->_calcHValue(sharedPoint, goal), sharedPoint);
					earlyExploration.push_back(newNode);
					closedList.insert(std::make_pair(t, true));
				}
			}
		}

		std::sort(earlyExploration.begin(), earlyExploration.end());
		if (!earlyExploration.empty() && earlyExploration.front().fCost < currentNode.fCost)
		{
			earlyExporationNode = earlyExploration.front();
			earlyExploration.erase(earlyExploration.begin());
		}
		openList.insert(openList.end(), earlyExploration.begin(), earlyExploration.end());
		earlyExploration.clear();
	}

	if (!nodes.empty())
	{
		currentNode = nodes[planB_nodeIndex];
		
		std::vector<Float2> finalPath;
		while (currentNode.parentNodeIndex != -1)
		{
			finalPath.push_back(currentNode.worldPosition);
			currentNode = nodes[currentNode.parentNodeIndex];
		}
		std::reverse(finalPath.begin(), finalPath.end());
		//finalPath.push_back(goal);
		return finalPath;
	}
	

	return std::vector<Float2>();
}

Pathfinding::Tile::Tile(const Float2& pos, const Float2& size, unsigned int depth, bool createChildren)
{
	Position = pos;
	Size = size;
	Depth = depth;

	D.SetPosition(Position.x, Position.y);
	D.SetSize({ Size.x, Size.y });
	D.SetOutlineThickness(-1);
	D.SetOutlineColor(255, 255, 255, 128);
	D.SetColor(sf::Color::Transparent);
	D.SetOrigin({ 0,0 });

	ObjectVector.insert_or_assign(		STATIC,  std::vector<PathfindingObject*>());
	ObjectVector.insert_or_assign(		DYNAMIC, std::vector<PathfindingObject*>());
	FamilyObjectVector.insert_or_assign(STATIC,  std::vector<PathfindingObject*>());
	FamilyObjectVector.insert_or_assign(DYNAMIC, std::vector<PathfindingObject*>());

	if (createChildren && depth < Max_Depth)
	{
		Float2 cSize = size * 0.5f;
		Float2 xOff(cSize.x, 0.0);
		Float2 yOff(0.0, cSize.y);

		pChildren[0] = new Tile(pos,		 cSize, depth + 1, createChildren);
		pChildren[0]->pParent = this;

		pChildren[1] = new Tile(pos + xOff,  cSize, depth + 1, createChildren);
		pChildren[1]->pParent = this;

		pChildren[2] = new Tile(pos + yOff,  cSize, depth + 1, createChildren);
		pChildren[2]->pParent = this;

		pChildren[3] = new Tile(pos + cSize, cSize, depth + 1, createChildren);
		pChildren[3]->pParent = this;

		HasChildren = true;
		AllocatedChildren = true;
	}
}

/* ================= TILE FUNCTIONS ===============*/
void Pathfinding::Tile::GetNeighbour(Tile* t, std::vector<Tile*> & vector)
{
	PathfindingObject to = PathfindingObject::CreateAABBHitbox(t->Size * 0.5f, t->Position + t->Size * 0.5f);
	PathfindingObject thisObj = PathfindingObject::CreateAABBHitbox(Size * 0.5f, Position + Size * 0.5f);

	if (to.Intersects(thisObj))
	{
		if (IsLeaf())
		{
			if (abs(Position.x - t->Position.x) < 0.01f || abs(Position.y == t->Position.y) < 0.01f)
			{
				// Potential Neighbour
			
				Float2 point = { t->Position.x + t->Size.x * 0.5f, t->Position.y + t->Size.y * 0.5f };
				bool inside = point.x > Position.x && point.x < Position.x + Size.x && point.y > Position.y&& point.y < Position.y + Size.y;

				if (!inside)
				{
					vector.push_back(this);
				}
			}
		}
		else
		{
			if (HasChildren)
			{
				for (int i = 0; i < 4; i++)
				{
					pChildren[i]->GetNeighbour(t, vector);
				}
			}
		}
	}
}

Pathfinding::Tile* Pathfinding::Tile::GetTile(const Float2& point)
{
	if (Inside(point))
	{
		if (IsLeaf())
			return this;
		else if (HasChildren)
		{
			Tile* r = nullptr;
			for (int i = 0; i < 4 && !r; i++)
			{
				r = pChildren[i]->GetTile(point);
			}

			return r;
		}
	}

	return nullptr;
}

void Pathfinding::Tile::GetNeighbour2(Tile* t, std::vector<Tile*>& vector, PathfindingObject* po)
{
	PathfindingObject to = PathfindingObject::CreateAABBHitbox(t->Size * 0.5f, t->Position + t->Size * 0.5f);
	PathfindingObject thisObj = PathfindingObject::CreateAABBHitbox(Size * 0.5f, Position + Size * 0.5f);
	Float2 poSize = po->GetHitboxSize();

	if (to.Intersects(thisObj))
	{
		if (IsLeaf())
		{
			PathfindingObject poTemp;

			switch (po->GetHitboxType())
			{
			case PathfindingObject::Circle:
				poTemp = PathfindingObject::CreateCircleHitbox(po->GetHitboxSize().x, Position + Size * 0.5f);
				break;

			case PathfindingObject::AABB:
				poTemp = PathfindingObject::CreateAABBHitbox(po->GetHitboxSize(), Position + Size * 0.5f);
				break;
				
			case PathfindingObject::Rectangle:
			default:
				poTemp = PathfindingObject::CreateRectangleHitbox(po->GetHitboxSize(), po->GetHitboxRotation(), Position + Size * 0.5f);
				break;
			}

			if (!s_singleton->ExistCollisionFor(&poTemp, STATIC_TYPE))
			{
				vector.push_back(this);
			}
		}		
		else
		{
			size_t vecSizeBefore = vector.size();
			for (int i = 0; i < 4; i++)
			{
				pChildren[i]->GetNeighbour2(t, vector, po);
			}
		}
	}
}

Pathfinding::Tile* Pathfinding::Tile::GetTile3(const Float2& point, const Float2& size)
{
	if (Inside(point))
	{
		if (IsLeaf())
			return this;
		else if (HasChildren)
		{
			Tile* r = nullptr;
			for (int i = 0; i < 4 && !r; i++)
			{
				r = pChildren[i]->GetTile3(point, size);
			}

			if (!r && size.x < Size.x * 0.5f && size.y < Size.y * 0.5f)
				return this;

			return r;
		}
	}

	return nullptr;
}

Pathfinding::Tile* Pathfinding::Tile::GetTile2(const Float2& point, PathfindingObject* po, PathfindingObject* ignoreCol)
{
	Float2 poSize = po->GetHitboxSize();

	if (Inside(point) && Size.x * 0.5f >= poSize.x && Size.y * 0.5f >= poSize.y)
	{
		if (IsLeaf())
		{
			bool isOk = true;
			for (auto& o : ObjectVector[STATIC])
			{
				if (o == ignoreCol || o == po)
					continue;
				if (o->Intersects(*po))
				{
					isOk = false;
					break;
				}
			}

			if (isOk)
				return this;
		}
		else if (pChildren[0]->Size.x * 0.5f < poSize.x && pChildren[0]->Size.y * 0.5f < poSize.y)
		{
			
			if (FamilyObjectVector[STATIC].size() < MAX_COMPARE_ALLOWED + 1U)
			{
				bool isOk = true;
				for (auto& o : FamilyObjectVector[STATIC])
				{
					if (o == po || o == ignoreCol)
						continue;
					if (o->Intersects(*po))
					{
						isOk = false;
						break;
					}
				}
				if (isOk)
					return this;
			}
		}
		else
		{
			Tile* r = nullptr;
			for (int i = 0; i < 4 && !r; i++)
			{
				r = pChildren[i]->GetTile2(point, po, ignoreCol);
			}
			return r;
		}
	}

	return nullptr;
}

void Pathfinding::Tile::Clean()
{
	FamilyObjectVector[STATIC].clear();
	FamilyObjectVector[DYNAMIC].clear();
	FamilyObjectVector.clear();

	ObjectVector[STATIC].clear();
	ObjectVector[DYNAMIC].clear();
	ObjectVector.clear();

	if (HasChildren)
	{
		for (int i = 0; i < 4; i++)
		{
			pChildren[i]->Clean();
			delete pChildren[i];
			pChildren[i] = nullptr;
		}
		HasChildren = false;
		AllocatedChildren = false;
	}
}

void Pathfinding::Tile::Clear()
{
	D.SetOutlineColor(255, 255, 255, 128);
	FamilyObjectVector[STATIC].clear();
	FamilyObjectVector[DYNAMIC].clear();

	ObjectVector[STATIC].clear();
	ObjectVector[DYNAMIC].clear();
	
	if (HasChildren)
	{
		for (int i = 0; i < 4; i++)
		{
			pChildren[i]->Clear();
		}
		HasChildren = true;
	}
}

void Pathfinding::Tile::Place(PathfindingObject* object)
{
	PathfindingObject aabb = PathfindingObject::CreateAABBHitbox(Size * 0.5f, Position + Size * 0.5f);
	if (object->Intersects(aabb))
	{
		bool isStatic = object->IsStatic();
		if (Depth == Max_Depth)
		{
			ObjectVector[isStatic].push_back(object);
		}
		else if (HasChildren)
		{
			FamilyObjectVector[isStatic].push_back(object);
			for (int i = 0; i < 4; i++)
				pChildren[i]->Place(object);
		}
		else
		{
			FamilyObjectVector[isStatic].push_back(object);
			Float2 childrenSize = Size;
			childrenSize.x *= 0.5f;
			childrenSize.y *= 0.5f;
			for (int y = 0; y < 2; y++)
			{
				Float2 childrenPos = Position;
				childrenPos.y += y * childrenSize.y;
				for (int x = 0; x < 2; x++)
				{
					childrenPos.x += x * childrenSize.x;
					unsigned int index = x + y * 2;
					if (!AllocatedChildren)
					{
						static long long int S = 0;
						pChildren[index] = new Pathfinding::Tile(childrenPos, childrenSize, Depth + 1);
					}
					pChildren[index]->pParent = this;
					pChildren[index]->Place(object);
				}
			}
			HasChildren = true;
			AllocatedChildren = true;
		}
	}
}

bool Pathfinding::Tile::Inside(const Float2& point)
{
	return point.x >= Position.x && point.x <= Position.x + Size.x &&
		point.y >= Position.y && point.y <= Position.y + Size.y;
}

PathfindingObject* Pathfinding::Tile::ExistCollisionFor(PathfindingObject* object, Pathfinding::COL_TYPE type)
{
	PathfindingObject aabb = PathfindingObject::CreateAABBHitbox(Size * 0.5f, Position + Size * 0.5f);
	if (object->Intersects(aabb))
	{
		if (IsLeaf())
		{
			if (type == COL_TYPE::ALL || type == COL_TYPE::STATIC_TYPE)
				for (auto& o : ObjectVector[STATIC])
				{
					if (o == object)
						continue;

					if (object->Intersects(*o))
						return o;
				}
			if (type == COL_TYPE::ALL || type == COL_TYPE::DYNAMIC_TYPE)
				for (auto& o : ObjectVector[DYNAMIC])
				{
					if (o == object)
						continue;

					if (object->Intersects(*o))
						return o;
				}
		}
		else if (HasChildren)
		{
			PathfindingObject * r = nullptr;
			for (int i = 0; i < 4 && !r; i++)
			{
				r = pChildren[i]->ExistCollisionFor(object, type);
			}
			return r;
		}
	}

	return nullptr;
}

void Pathfinding::Tile::GetAllObjectsCollidingWith(PathfindingObject* object, std::vector<PathfindingObject*>& vector, Pathfinding::COL_TYPE type)
{
	PathfindingObject aabb = PathfindingObject::CreateAABBHitbox(Size * 0.5f, Position + Size * 0.5f);
	if (object->Intersects(aabb))
	{
		if (IsLeaf())
		{
			if (type == COL_TYPE::ALL || type == COL_TYPE::STATIC_TYPE)
				for (auto& o : ObjectVector[STATIC])
				{
					if (o == object)
						continue;

					if (object->Intersects(*o))
					{
						if (std::find(vector.begin(), vector.end(), o) == vector.end())
							vector.push_back(o);
					}
				}
			if (type == COL_TYPE::ALL || type == COL_TYPE::DYNAMIC_TYPE)
				for (auto& o : ObjectVector[DYNAMIC])
				{
					if (o == object)
						continue;

					if (std::find(vector.begin(), vector.end(), o) == vector.end())
						vector.push_back(o);
				}
		}
		else if (HasChildren)
		{
			for (int i = 0; i < 4; i++)
			{
				pChildren[i]->GetAllObjectsCollidingWith(object, vector, type);
			}
		}
	}
}

bool Pathfinding::Tile::IsLeaf()
{
	return !HasChildren;
}

bool Pathfinding::Tile::IsEmpty()
{
	return ObjectVector[STATIC].empty() && ObjectVector[DYNAMIC].empty();
}


void Pathfinding::Tile::Draw()
{
	//if (IsLeaf())
	if (Depth == DRAW_DEPTH)
		D.Draw();
	else if (DRAW_DEPTH == Max_Depth + 1 && IsLeaf())
		D.Draw();

	if (HasChildren)
	{
		for (int i = 0; i < 4; i++)
		{
			pChildren[i]->Draw();
		}
	}

}
