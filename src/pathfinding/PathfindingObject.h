#ifndef _PATHFINDING_OBJECT_H_
#define _PATHFINDING_OBJECT_H_

#include <vector>
#include <mutex>
#include "../util/Structs.h"
#include "../util/Timer.h"
#include <Box2D/Box2D.h>
#include <stdio.h>
#include "../util/Util.h"

class World;

class PathfindingObject
{
public:
	enum HITBOX_TYPE
	{
		Circle,
		Rectangle,
		AABB
	};
	enum PATHING_TYPE
	{
		LAND,
		LAND_ALL,
		WATER,
		WATER_AND_LAND,
		WATER_AND_LAND_ALL,
		FLYING
	};
public:
	static float WAIT_FOR_PATH_REQUEST;

	PathfindingObject();
	virtual ~PathfindingObject();

	void SetCircleHitbox(float r);
	void SetRectangleHitbox(const Float2& size, float rotation);
	void SetAABBHitbox(const Float2& size);

	static PathfindingObject CreateCircleHitbox(float r, const Float2 & position);
	static PathfindingObject CreateRectangleHitbox(const Float2& size, float rotation, const Float2& position);
	static PathfindingObject CreateAABBHitbox(const Float2& size, const Float2& position);

	void SetStatic(bool flag = true);
	bool IsStatic();

	void PlaceInWorld();
	void RemoveFromWorld();

	virtual void SetPosition(const Float2& position);
	virtual void SetSize(const Float2& size);
	virtual void SetRotation(float rotation);

	void SetLinearVelocity(const Float2& vel);
	Float2 GetLinearVelocity();

	void UpdateFromPhysics();

	void RequestSingleAgentPath(const Float2 & destinationPoint);
	void SetPath(const std::vector<Float2>& path);

	std::vector<Float2> GetSingleAgentPath();
	
	bool ReadyForDeletion();
	bool CanRequestPath() { return !m_isStatic && !m_unableToMove; }
	void ActivateMovement(bool state = true) { m_unableToMove = !state; }

	const Float2& GetHitboxPosition() const;
	const Float2& GetHitboxSize() const;
	float GetHitboxRotation() const;

	HITBOX_TYPE GetHitboxType() const { return m_hitboxType; }

	Float2 GetClosestPointFrom(const Float2& point);

	bool Intersects(const PathfindingObject& po);
	bool Contains(const Float2& point);

	void SetPathingType(PATHING_TYPE type) { m_pathingType = type; }
	PATHING_TYPE GetPathingType() const { return m_pathingType; }

	b2BodyDef*		GetBodyDefinition() { return &m_bodyDef; }
	b2Body*			GetBody() { return m_body; }
	b2Shape*		GetShape() { return m_shape; }
	b2FixtureDef*	GetFixtureDefinition() { return &m_fixtureDef; }

private:
	static bool _circleCircleIntersection(const PathfindingObject& o1, const PathfindingObject& o2);
	static bool _circleAABBIntersection(const PathfindingObject& o1, const PathfindingObject& o2);
	static bool _circleRectangleIntersection(const PathfindingObject& o1, const PathfindingObject& o2);
	static bool _AABBAABBIntersection(const PathfindingObject& o1, const PathfindingObject& o2);
	static bool _AABBRectangleIntersection(const PathfindingObject& o1, const PathfindingObject& o2);
	static bool _rectangleRectangleIntersection(const PathfindingObject& o1, const PathfindingObject& o2);

private:
	Float2 m_position;
	Float2 m_size;
	float m_rotation;
	Timer m_pathRequestTimer;

protected:
	HITBOX_TYPE m_hitboxType;
	
	bool m_isRequestingPath;
	bool m_isNewPathAvailable;
	bool m_tempPath;
	bool m_isStatic;
	bool m_unableToMove;

	std::vector<Float2> m_path;

	b2BodyDef m_bodyDef;
	b2Body* m_body = nullptr;
	b2Shape * m_shape = nullptr;
	b2FixtureDef m_fixtureDef;

	PATHING_TYPE m_pathingType = LAND;

};

#endif