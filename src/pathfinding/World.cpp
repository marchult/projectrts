#include "World.h"
#include "pathfinding.h"
#include "../util/Util.h"

World* World::m_singleton = nullptr;
float World::LINEAR_DAMPNING = 5.0f;
float World::ANGULAR_DAMPNING = 5.0f;
float World::WORLD_SIZE_X = 0.0f;
float World::WORLD_SIZE_Y = 0.0f;

World::World()
{
	m_world = new b2World({0.0f, 0.0f});
	m_frequency = (1.0 / 128.0);
	m_velIt = 6;
	m_posIt = 2;
	m_isRunning = false;
	_setupWorldBorder();
}

World::~World()
{
	m_deletedWorldObjects.clear();
	m_newWorldObjects.clear();
	m_existingWorldObjects.clear();
	delete m_world;
	m_world = nullptr;
}

World* World::GetInstance()
{
	return m_singleton;
}

void World::Init()
{
	if (!m_singleton)
	{
		m_singleton = new World();
	}
}

void World::Release()
{
	m_singleton->m_isRunning = false;

	_spinlock(); // Spinlock so that we can terminate the thread

	m_singleton->m_thread.join();

	delete m_singleton;
	m_singleton = nullptr;
}

void World::AddObject(PathfindingObject* pObj)
{
	m_newWorldObjects.insert_or_assign(pObj, true);
}

void World::RemoveObject(PathfindingObject* pObj)
{
	m_deletedWorldObjects.insert_or_assign(pObj, true);
}

void World::Update()
{
	for (auto& pair : m_newWorldObjects)
	{
		m_existingWorldObjects.insert_or_assign(pair.first, pair.second);
	}

	for (auto& pair : m_deletedWorldObjects)
	{
		m_existingWorldObjects.erase(pair.first);
	}
	m_newWorldObjects.clear();
	m_deletedWorldObjects.clear();
	std::vector<PathfindingObject*> worldObjects(m_existingWorldObjects.size());
	long unsigned int counter = 0;
	for (auto& pair : m_existingWorldObjects)
	{
		worldObjects[counter++] = pair.first;
	}

	Pathfinding::GetInstance()->Rebuild(worldObjects);
}

void World::Step(float dt)
{
	m_world->Step(dt, m_velIt, m_posIt);
}

void World::StartPhysicsThread()
{
	StartPhysicsThread(m_frequency, m_velIt, m_posIt);
}

void World::StartPhysicsThread(float32 frequencyInSeconds,
	int32 velocityIterations, int32 positionIterations)
{
	if (!m_isRunning)
	{
		m_frequency = frequencyInSeconds;
		m_velIt = velocityIterations;
		m_posIt = positionIterations;

		m_thread = std::thread(&World::_physicsThread, this);
	}
}

Region::Region_Type World::GetRegionAt(unsigned int x, unsigned int y) const
{
	return m_terrainCreator.GetRegionAt(x, y);
}

bool World::IsType(unsigned int x, unsigned int y, Region::Region_Type t) const
{
	return m_terrainCreator.IsType(x, y, t);
}

const std::vector<Region::Line>& World::GetEdges() const
{
	return m_terrainCreator.GetEdges();
}

void World::CreateRandomWorld(unsigned int size, TerrainCreator::Terrain_Type t)
{
	m_terrainCreator.Create(size, t);

	auto edges = m_terrainCreator.GetEdges();
	m_terrainBorder.resize(edges.size());
	size_t counter = 0;
	for (auto& e : edges)
	{
		Terrain_Border* tb = &m_terrainBorder[counter++];
		tb->BodyDef.type = b2_staticBody;
		//tb->BodyDef.position = Convert::ConvertToB2Vec(e.b);
		tb->Body = m_world->CreateBody(&tb->BodyDef);
		tb->Shape.Set(Convert::ConvertToB2Vec(e.a), Convert::ConvertToB2Vec(e.b));
		//tb->Body->CreateFixture(&tb->Shape, 0.0);
		tb->FixtureDef.shape = &tb->Shape;
		tb->FixtureDef.density = 1.0f;
		tb->FixtureDef.density = 0.3f;
		tb->Body->CreateFixture(&tb->FixtureDef);
	}
}

void World::LoadWorldFromHeightmap(const std::string& hmPath)
{
	m_terrainCreator.LoadFromFile(hmPath);
}

sf::Texture* World::GetTerrainTexture()
{
	return m_terrainCreator.GetTexture();
}

void World::_physicsThread()
{
	m_isRunning = true;
	b2Timer t;
	double time = 0.0;
	double fq = m_frequency * 1000;
	while (m_isRunning)
	{
		while (time > fq)
		{
			time -= fq;
			m_world->Step(m_frequency, m_velIt, m_posIt);
		}
		time += t.GetMilliseconds();
		t.Reset();
	}
}

#pragma optimize( "", off)
void World::_spinlock()
{
	while (!m_singleton->m_thread.joinable());
}
#pragma optimize( "", on)

void World::_setupWorldBorder()
{
	const int LEFT = 0;
	const int TOP = 1;
	const int RIGHT = 2;
	const int BOT = 3;


	Float2 wStart = Pathfinding::GetInstance()->GetWorldStart();
	Float2 wEnd = Pathfinding::GetInstance()->GetWorldEnd();

	m_worldBorder[LEFT].BodyDef.type = b2_staticBody;
	m_worldBorder[LEFT].BodyDef.position.Set(wStart.x - 5, wStart.y);
	m_worldBorder[LEFT].Body = m_world->CreateBody(&m_worldBorder[LEFT].BodyDef);
	m_worldBorder[LEFT].Shape.SetAsBox(5, wEnd.y);
	m_worldBorder[LEFT].Body->CreateFixture(&m_worldBorder[LEFT].Shape, 0.0f);
	m_worldBorder[LEFT].FixtureDef.shape = &m_worldBorder[LEFT].Shape;
	m_worldBorder[LEFT].FixtureDef.density = 1.0f;
	m_worldBorder[LEFT].FixtureDef.friction = 0.3f;
	m_worldBorder[LEFT].Body->CreateFixture(&m_worldBorder[LEFT].FixtureDef);

	m_worldBorder[TOP].BodyDef.type = b2_staticBody;
	m_worldBorder[TOP].BodyDef.position.Set(wStart.x, wStart.y - 5);
	m_worldBorder[TOP].Body = m_world->CreateBody(&m_worldBorder[TOP].BodyDef);
	m_worldBorder[TOP].Shape.SetAsBox(wEnd.x, 5);
	m_worldBorder[TOP].Body->CreateFixture(&m_worldBorder[TOP].Shape, 0.0f);
	m_worldBorder[TOP].FixtureDef.shape = &m_worldBorder[TOP].Shape;
	m_worldBorder[TOP].FixtureDef.density = 1.0f;
	m_worldBorder[TOP].FixtureDef.friction = 0.3f;
	m_worldBorder[TOP].Body->CreateFixture(&m_worldBorder[TOP].FixtureDef);

	m_worldBorder[RIGHT].BodyDef.type = b2_staticBody;
	m_worldBorder[RIGHT].BodyDef.position.Set(wEnd.x, wStart.y);
	m_worldBorder[RIGHT].Body = m_world->CreateBody(&m_worldBorder[RIGHT].BodyDef);
	m_worldBorder[RIGHT].Shape.SetAsBox(5, wEnd.y);
	m_worldBorder[RIGHT].Body->CreateFixture(&m_worldBorder[RIGHT].Shape, 0.0f);
	m_worldBorder[RIGHT].FixtureDef.shape = &m_worldBorder[RIGHT].Shape;
	m_worldBorder[RIGHT].FixtureDef.density = 1.0f;
	m_worldBorder[RIGHT].FixtureDef.friction = 0.3f;
	m_worldBorder[RIGHT].Body->CreateFixture(&m_worldBorder[RIGHT].FixtureDef);

	m_worldBorder[BOT].BodyDef.type = b2_staticBody;
	m_worldBorder[BOT].BodyDef.position.Set(wStart.x, wEnd.y);
	m_worldBorder[BOT].Body = m_world->CreateBody(&m_worldBorder[BOT].BodyDef);
	m_worldBorder[BOT].Shape.SetAsBox(wEnd.x, 5);
	m_worldBorder[BOT].Body->CreateFixture(&m_worldBorder[BOT].Shape, 0.0f);
	m_worldBorder[BOT].FixtureDef.shape = &m_worldBorder[BOT].Shape;
	m_worldBorder[BOT].FixtureDef.density = 1.0f;
	m_worldBorder[BOT].FixtureDef.friction = 0.3f;
	m_worldBorder[BOT].Body->CreateFixture(&m_worldBorder[BOT].FixtureDef);
}

RayTracerAll::RayTracerAll(const Float2& startPoint)
{
	m_startPoint = startPoint;
}

float32 RayTracerAll::ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction)
{
	m_contacts.insert(std::make_pair((Convert::ConvertToFloat2(point) - m_startPoint).length(),fixture));
	return 1;
}

std::vector<std::pair<b2Fixture*, float>> RayTracerAll::GetAllContacts()
{
	std::vector<std::pair<b2Fixture*, float>> contacts(m_contacts.size());
	int i = 0;
	for (auto& c : m_contacts)
	{
		contacts[i].first = c.second;
		contacts[i].second = c.first;
		i++;
	}
	return contacts;
}
