#ifndef _WORLD_H_
#define _WORLD_H_

#include <map>
#include <unordered_map>
#include <Box2D/Box2D.h>
#include <stdio.h>
#include <thread>
#include "../Terrain_Generation/TerrainCreator.h"
#include <vector>

class PathfindingObject;

class RayTracerAll : public b2RayCastCallback
{
public:
	RayTracerAll(const Float2& startPoint);

	float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point,
		const b2Vec2& normal, float32 fraction) override;

	bool HasContacts() { return !m_contacts.empty(); }
	b2Fixture* GetClosestContact() { for (auto& c : m_contacts) return c.second; }
	std::vector<std::pair<b2Fixture*, float>> GetAllContacts();

private:
	std::map<float, b2Fixture*> m_contacts;
	Float2 m_startPoint;
};

class World
{
private:
	World();
	~World();
public:
	static float LINEAR_DAMPNING;
	static float ANGULAR_DAMPNING;

	static float WORLD_SIZE_X;
	static float WORLD_SIZE_Y;

	static World* GetInstance();
	static void Init();
	static void Release();

	void AddObject(PathfindingObject* pObj);
	void RemoveObject(PathfindingObject* pObj);
	void Update();
	void Step(float dt);
	void StartPhysicsThread();
	void StartPhysicsThread(float32 frequencyInSeconds,
		int32 velocityIterations = 6, int32 positionIterations = 2);
	b2World* Getb2World() { return m_world; }

	Region::Region_Type GetRegionAt(unsigned int x, unsigned int y) const;
	bool IsType(unsigned int x, unsigned int y, Region::Region_Type t) const;
	const std::vector<Region::Line>& GetEdges() const;
	void CreateRandomWorld(unsigned int size, TerrainCreator::Terrain_Type t = TerrainCreator::Random);
	void LoadWorldFromHeightmap(const std::string & hmPath);

	sf::Texture* GetTerrainTexture();

private:
	void _physicsThread();
	static void _spinlock();
	void _setupWorldBorder();

private:
	static World * m_singleton;

	struct World_Border
	{
		b2BodyDef BodyDef;
		b2Body* Body = nullptr;
		b2PolygonShape Shape;
		b2FixtureDef FixtureDef;
	};

	struct Terrain_Border
	{
		b2BodyDef BodyDef;
		b2Body* Body = nullptr;
		b2EdgeShape Shape;
		b2FixtureDef FixtureDef;
	};

	std::map<PathfindingObject*, bool> m_existingWorldObjects;
	std::unordered_map<PathfindingObject*, bool> m_newWorldObjects;
	std::unordered_map<PathfindingObject*, bool> m_deletedWorldObjects;

	World_Border m_worldBorder[4];
	std::vector<Terrain_Border> m_terrainBorder;


	std::thread m_thread;
	bool m_isRunning;
	b2World * m_world = nullptr;
	float32 m_frequency;
	int32 m_velIt;
	int32 m_posIt;

	TerrainCreator m_terrainCreator;
};

#endif // !_WORLD_H_