#include "PathfindingObject.h"

#include <algorithm>
#include "World.h"
#include "pathfinding.h"


float PathfindingObject::WAIT_FOR_PATH_REQUEST = 0.5f;

PathfindingObject::PathfindingObject()
{
	m_position = { 0.0f, 0.0f };
	m_size = { 1.0f, 1.0f };
	m_hitboxType = Circle;
	m_rotation = 0;
	m_isRequestingPath = false;
	m_isNewPathAvailable = false;
	m_isStatic = false;
	m_unableToMove = false;
	m_tempPath = false;
	m_pathRequestTimer.Start();
}

PathfindingObject::~PathfindingObject()
{

}

void PathfindingObject::SetCircleHitbox(float r)
{
	m_size.x = r;
	m_size.y = r;
	m_hitboxType = Circle;
}

void PathfindingObject::SetRectangleHitbox(const Float2& size, float rotation)
{
	m_size = size;
	m_rotation = rotation;
	m_hitboxType = Rectangle;
}

void PathfindingObject::SetAABBHitbox(const Float2& size)
{
	m_size = size;
	m_hitboxType = AABB;
}

PathfindingObject PathfindingObject::CreateCircleHitbox(float r, const Float2& position)
{
	PathfindingObject po;
	po.m_position = position;
	po.m_size.x = r;
	po.m_size.y = r;
	po.m_hitboxType = Circle;
	return po;
}

PathfindingObject PathfindingObject::CreateRectangleHitbox(const Float2& size, float rotation, const Float2& position)
{
	PathfindingObject po;
	po.m_size = size;
	po.m_position = position;
	po.m_rotation = rotation;
	po.m_hitboxType = Rectangle;
	return po;
}

PathfindingObject PathfindingObject::CreateAABBHitbox(const Float2& size, const Float2& position)
{
	PathfindingObject po;
	po.m_size = size;
	po.m_position = position;
	po.m_hitboxType = AABB;
	return po;
}

void PathfindingObject::SetStatic(bool flag)
{
	m_isStatic = flag;
}

bool PathfindingObject::IsStatic()
{
	return m_isStatic;
}

void PathfindingObject::PlaceInWorld()
{
	World* w = World::GetInstance();
	b2World* b2W = w->Getb2World();

	if (!m_body)
	{
		m_bodyDef.linearDamping = World::LINEAR_DAMPNING;
		m_bodyDef.angularDamping = World::ANGULAR_DAMPNING;

		if (m_hitboxType == AABB)
		{
			m_bodyDef.fixedRotation = true;
		}
		else
		{
			m_bodyDef.angle = m_rotation;
		}
		

		m_bodyDef.position.Set(m_position.x, m_position.y);
		if (!m_isStatic)
			m_bodyDef.type = b2_dynamicBody;
		m_body = b2W->CreateBody(&m_bodyDef);

		switch (m_hitboxType)
		{
		case PathfindingObject::Circle:
		{
			m_shape = new b2CircleShape();
			b2CircleShape* c = (b2CircleShape*)m_shape;
			c->m_radius = m_size.x;
			break;
		}
		default:
		{
			m_shape = new b2PolygonShape();
			b2PolygonShape* p = (b2PolygonShape*)m_shape;
			p->SetAsBox(m_size.x, m_size.y);
			break;
		}
		}

		if (m_isStatic)
		{
			m_body->CreateFixture(m_shape, 0.0f);
		}
		else
		{
			m_fixtureDef.shape = m_shape;
			m_fixtureDef.density = 1.0f;
			m_fixtureDef.friction = 0.3;
			m_body->CreateFixture(&m_fixtureDef);
		}
	}

	w->AddObject(this);
}

void PathfindingObject::RemoveFromWorld()
{
	World* w = World::GetInstance();
	b2World* w2 = w->Getb2World();

	if (m_body)
		w2->DestroyBody(m_body);

	m_body = nullptr;
	delete m_shape;
	m_shape = nullptr;

	w->RemoveObject(this);
}

void PathfindingObject::SetPosition(const Float2& position)
{
	m_position = position;
	if (m_body)
		m_body->SetTransform(
			Convert::ConvertToB2Vec(m_position),
			m_rotation);
}

void PathfindingObject::SetSize(const Float2& size)
{
	m_size = size;
	if (m_hitboxType == Circle)
	{
		m_size.y = m_size.x;
		if (m_shape)
			((b2CircleShape*)m_shape)->m_radius = m_size.y;
	}
	else
	{
		if (m_shape)
			((b2PolygonShape*)m_shape)->SetAsBox(m_size.x, m_size.y);
	}

}

void PathfindingObject::SetRotation(float rotation)
{
	m_rotation = rotation;
	if (m_body)
		m_body->SetTransform(
			Convert::ConvertToB2Vec(m_position), 
			rotation);
}

void PathfindingObject::SetLinearVelocity(const Float2& vel)
{
	if (m_body)
		m_body->SetLinearVelocity(Convert::ConvertToB2Vec(vel));
}

Float2 PathfindingObject::GetLinearVelocity()
{
	if (m_body)
		return Convert::ConvertToFloat2(m_body->GetLinearVelocity());
	return Float2();
}

void PathfindingObject::UpdateFromPhysics()
{
	if (m_body)
	{
		if (m_hitboxType != AABB)
			m_rotation = m_body->GetAngle();
		
		m_position = Convert::ConvertToFloat2(m_body->GetPosition());
	}
}

void PathfindingObject::RequestSingleAgentPath(const Float2& destinationPoint)
{
	if (!m_isStatic && !m_unableToMove)
	{
		if (!m_isRequestingPath && m_pathRequestTimer.GetElapsedTime() > WAIT_FOR_PATH_REQUEST)
		{
			m_path.clear();
			m_path.push_back(destinationPoint);
			m_tempPath = true;

			Float2 from = m_position;
			//if (!m_path.empty())
				//from = m_path.front();

			Pathfinding::GetInstance()->RequestSingleAgentPath(this, from, destinationPoint);
			m_isRequestingPath = true;
			m_pathRequestTimer.Restart();
		}
	}
}

void PathfindingObject::SetPath(const std::vector<Float2>& path)
{
	m_path = path;
	m_isRequestingPath = false;
	m_isNewPathAvailable = true;
}

std::vector<Float2> PathfindingObject::GetSingleAgentPath()
{
	//std::unique_lock<std::mutex> lock(m_pathMutex);
	return m_path;
}

bool PathfindingObject::ReadyForDeletion()
{
	return !m_isRequestingPath;
}

const Float2& PathfindingObject::GetHitboxPosition() const
{
	return m_position;
}

const Float2& PathfindingObject::GetHitboxSize() const
{
	return m_size;
}

float PathfindingObject::GetHitboxRotation() const
{
	return m_rotation;
}

Float2 PathfindingObject::GetClosestPointFrom(const Float2& point)
{
	switch (m_hitboxType)
	{
	case PathfindingObject::Rectangle:
		return Float2(0, 0);
		break;
	case PathfindingObject::AABB:
		{
			
			Float2 aabbSize = m_size;
			Float2 pos = point;
			Float2 aabbPos = m_position - aabbSize;
			aabbSize = aabbSize * 2;
		
			if (pos.x > aabbPos.x&& pos.x < aabbPos.x + aabbSize.x &&
				pos.y > aabbPos.y&& pos.y < aabbPos.y + aabbSize.y)
			{
				float leftDist = abs(pos.x - aabbPos.x);
				float rightDist = abs(pos.x - (aabbPos.x + aabbSize.x));
				float topDist = abs(pos.y - aabbPos.y);
				float botDist = abs(pos.y - (aabbPos.y + aabbSize.y));

				if (leftDist < rightDist && leftDist < topDist && leftDist < botDist)
					return Float2(aabbPos.x, point.y);
				else if (rightDist < leftDist && rightDist < topDist && rightDist < botDist)
					return Float2(aabbPos.x + aabbSize.x, point.y);
				else if (topDist < leftDist && topDist < rightDist && topDist < botDist)
					return Float2(point.x, aabbPos.y);
				else 
					return Float2(point.x, aabbPos.y + aabbSize.y);

			}
			else
			{
				Float2 delta;
				delta.x = std::max(aabbPos.x, std::min(pos.x, aabbPos.x + aabbSize.x));
				delta.y = std::max(aabbPos.y, std::min(pos.y, aabbPos.y + aabbSize.y));

				return delta;
			}
		}
	case PathfindingObject::Circle:
	default:
		{
			Float2 pos = m_position;
			Float2 posToPoint = point - pos;
			return pos + posToPoint.normalize() * m_size.x;
		}
	}
}

bool PathfindingObject::Intersects(const PathfindingObject& po)
{
	bool intersects = false;
	if (m_hitboxType == Circle || po.m_hitboxType == Circle)
	{
		if (m_hitboxType == Circle)
		{
			switch (po.m_hitboxType)
			{
			case Circle:
				intersects = _circleCircleIntersection(*this, po);
				break;
			case Rectangle:
				intersects = _circleRectangleIntersection(*this, po);
				break;
			case AABB:
				intersects = _circleAABBIntersection(*this, po);
				break;
			}
		}
		else
		{
			switch (m_hitboxType)
			{
			case Circle:
				intersects = _circleCircleIntersection(po, *this);
				break;
			case Rectangle:
				intersects = _circleRectangleIntersection(po, *this);
				break;
			case AABB:
				intersects = _circleAABBIntersection(po, *this);
				break;
			}
		}
	}
	else if (m_hitboxType == AABB || po.m_hitboxType == AABB)
	{
		if (m_hitboxType == AABB)
		{
			switch (po.m_hitboxType)
			{
			case Circle:
				intersects = _circleAABBIntersection(po, *this);
				break;
			case Rectangle:
				intersects = _AABBRectangleIntersection(*this, po);
				break;
			case AABB:
				intersects = _AABBAABBIntersection(*this, po);
				break;
			}
		}
		else
		{
			switch (m_hitboxType)
			{
			case Circle:
				intersects = _circleAABBIntersection(*this, po);
				break;
			case Rectangle:
				intersects = _AABBRectangleIntersection(po, *this);
				break;
			case AABB:
				intersects = _AABBAABBIntersection(*this, po);
				break;
			}
		}
	}
	else
	{
		if (m_hitboxType == Rectangle)
		{
			switch (po.m_hitboxType)
			{
			case Circle:
				intersects = _circleRectangleIntersection(po, *this);
				break;
			case Rectangle:
				intersects = _rectangleRectangleIntersection(*this, po);
				break;
			case AABB:
				intersects = _AABBRectangleIntersection(po, *this);
				break;
			}
		}
		else
		{
			switch (m_hitboxType)
			{
			case Circle:
				intersects = _circleAABBIntersection(*this, po);
				break;
			case Rectangle:
				intersects = _rectangleRectangleIntersection(po, *this);
				break;
			case AABB:
				intersects = _AABBRectangleIntersection(*this, po);
				break;
			}
		}
	}

	return intersects;
}

bool PathfindingObject::Contains(const Float2& point)
{
	if (m_shape)
		return m_shape->TestPoint(m_body->GetTransform(), Convert::ConvertToB2Vec(point));
	else
	{
		switch (m_hitboxType)
		{
		case PathfindingObject::Circle:
			return (m_position - point).lengthSq() < m_size.x * m_size.x;
			break;
		case PathfindingObject::Rectangle:
			break;
		case PathfindingObject::AABB:
			return point.x > m_position.x - m_size.x && point.x < m_position.x + m_size.x &&
				point.y > m_position.y - m_size.y && point.y < m_position.y + m_size.y;
			break;
		}
	}
}

bool PathfindingObject::_circleCircleIntersection(const PathfindingObject& o1, const PathfindingObject& o2)
{
	float radius = o1.GetHitboxSize().x > o2.GetHitboxSize().x ? o1.GetHitboxSize().x : o2.GetHitboxSize().x;

	Float2 direction;
	direction.x = o2.GetHitboxPosition().x - o1.GetHitboxPosition().x;
	direction.y = o2.GetHitboxPosition().y - o1.GetHitboxPosition().y;

	float lSq = direction.x * direction.x + direction.y * direction.y;
	float rSq = radius * radius;

	return lSq < rSq;
}

bool PathfindingObject::_circleAABBIntersection(const PathfindingObject& o1, const PathfindingObject& o2)
{
	Float2 aabbSize = o2.GetHitboxSize();

	Float2 circlePos = o1.GetHitboxPosition();
	Float2 aabbPos = o2.GetHitboxPosition() - aabbSize;
	aabbSize = aabbSize * 2;
	float rad = o1.GetHitboxSize().x;

	float DeltaX = circlePos.x - std::max(aabbPos.x, std::min(circlePos.x, aabbPos.x + aabbSize.x));
	float DeltaY = circlePos.y - std::max(aabbPos.y, std::min(circlePos.y, aabbPos.y + aabbSize.y));
	return (DeltaX * DeltaX + DeltaY * DeltaY) < (rad * rad);
}

bool PathfindingObject::_circleRectangleIntersection(const PathfindingObject& o1, const PathfindingObject& o2)
{
	
	/*
	function pointInRotatedRectangle(pointX, pointY,
	rectX, rectY, rectOffsetX, rectOffsetY, rectWidth, rectHeight, rectAngle
) {
	var relX = pointX - rectX;
	var relY = pointY - rectY;
	var angle = -rectAngle;
	var angleCos = Math.cos(angle);
	var angleSin = Math.sin(angle);
	var localX = angleCos * relX - angleSin * relY;
	var localY = angleSin * relX + angleCos * relY;
	return localX >= -rectOffsetX && localX <= rectWidth - rectOffsetX &&
		   localY >= -rectOffsetY && localY <= rectHeight - rectOffsetY;
}

	*/

	Float2 shape = o1.GetHitboxPosition();
	Float2 rect = o2.GetHitboxPosition();
	Float2 size = o2.GetHitboxSize();
	float angle = -o2.GetHitboxRotation();
	Float2 rel = shape - rect;
	float angleCos = std::cos(angle);
	float angleSin = std::sin(angle);
	Float2 local;
	local.x = angleCos * rel.x - angleSin * rel.y;
	local.y = angleSin * rel.x + angleCos * rel.y;
	Float2 offset = o2.GetHitboxSize();

	Float2 delta;
	delta.x = std::max(-offset.x, std::min(local.x, size.x - offset.x));
	delta.y = std::max(-offset.y, std::min(local.y, size.y - offset.y));

	return delta.lengthSq() < (o1.GetHitboxSize().x * o1.GetHitboxSize().x);
}

bool PathfindingObject::_AABBAABBIntersection(const PathfindingObject& o1, const PathfindingObject& o2)
{
	Float2 o1Siz = o1.GetHitboxSize();
	Float2 o2Siz = o2.GetHitboxSize();
	Float2 o1Pos = o1.GetHitboxPosition() -o1Siz;
	Float2 o2Pos = o2.GetHitboxPosition() -o2Siz;
	o1Siz = o1Siz * 2;
	o2Siz = o2Siz * 2;


	return	o1Pos.x < o2Pos.x + o2Siz.x &&
			o1Pos.x + o1Siz.x > o2Pos.x &&
			o1Pos.y < o2Pos.y + o2Siz.y &&
			o1Pos.y + o1Siz.y > o2Pos.y;
}

bool PathfindingObject::_AABBRectangleIntersection(const PathfindingObject& o1, const PathfindingObject& o2)
{
	return _rectangleRectangleIntersection(o1, o2);
}

bool PathfindingObject::_rectangleRectangleIntersection(const PathfindingObject& o1, const PathfindingObject& o2)
{
	/*
	
		src: https://stackoverflow.com/questions/10962379/how-to-check-intersection-between-2-rotated-rectangles
	
	*/

	Float2 A, B, C, BL, TR;
	float ang = o1.m_rotation - o2.m_rotation;
	float cosa = cos(ang);
	float sina = sin(ang);
	float t, x, a;
	float dx;
	float ext1, ext2;

	C = o2.m_position;
	C = C - o1.m_position;
	C = C.Rotate(o2.m_rotation);

	BL = TR = C;
	BL = BL - o2.m_size * 2.0f;
	TR = TR + o2.m_size * 2.0f;
	BL.x -= o2.m_size.x;	BL.y -= o2.m_size.y;
	TR.x += o2.m_size.x;	TR.y += o2.m_size.y;

	A.x = -(o1.m_size.y) * sina; B.x = A.x; t = (o1.m_size.x) * cosa; A.x += t; B.x -= t;
	A.y = (o1.m_size.y) * cosa; B.y = A.y; t = (o1.m_size.x) * sina; A.y += t; B.y -= t;

	A.x = -o1.m_size.y * 0.5f * sina; B.x = A.x; t = o1.m_size.x * 0.5f * cosa; A.x += t; B.x -= t;
	A.y = o1.m_size.y * 0.5f * cosa; B.y = A.y; t = o1.m_size.x * 0.5f * sina; A.y += t; B.y -= t;

	t = sina * cosa;

	if (t < 0)
	{
		t = A.x; A.x = B.x; B.x = t;
		t = A.y; A.y = B.y; B.y = t;
	}

	if (sina < 0) { B.x = -B.x; B.y = -B.y; }

	if (B.x > TR.x || B.x > -BL.x) return false;

	if (t == 0)
	{
		ext1 = A.y; ext2 = -ext1;
	}
	else
	{
		x = BL.x - A.x; a = TR.x - A.x;
		ext1 = A.y;
		
		if (a * x > 0)
		{
			dx = A.x;
			if (x < 0) { dx -= B.x; ext1 -= B.y; x = a; }
			else { dx += B.x; ext1 += B.y; }
			ext1 *= x; ext1 /= dx; ext1 += A.y;
		}

		x = BL.x + A.x; a = TR.x + A.x;
		ext2 = -A.y;
		
		if (a * x > 0)
		{
			dx = -A.x;
			if (x < 0) { dx -= B.x; ext2 -= B.y; x = a; }
			else { dx += B.x; ext2 += B.y; }
			ext2 *= x; ext2 /= dx; ext2 -= A.y;
		}
	}

	return !((ext1 < BL.y && ext2 < BL.y) ||
		(ext1 > TR.y&& ext2 > TR.y));
}
