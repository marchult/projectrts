#include "SFMLRenderer.h"
#include "../game/entity/Entity.h"
//#include "drawable/SfmlDrawable.h"
#include "drawable/SfmlLine.h"
#include "../util/Util.h"
#include "SFMLTextureLoader.h"
#include "../src/pathfinding/World.h"

Float2 SFML_Renderer::GetMousePosition()
{
	sf::Vector2i mousePos = sf::Mouse::getPosition(m_window);
	
	Float2 mp;
	mp.x = mousePos.x;
	mp.y = mousePos.y;
	
	return mp;
}

Float2 SFML_Renderer::GetMouseWorldPosition()
{
	Float2 mp = GetMousePosition();
	Camera* c = Camera::GetActiveCamera();

	if (c)
	{
		auto wSizeI = m_window.getSize();
	
		Float2 wSize(wSizeI.x, wSizeI.y);
		Float2 wCenter = wSize * 0.5f;
		mp = mp - wCenter; // Screen position from center to mp;
		float rot =  Convert::DegreeseToRadians(c->GetRotation());
		float x = mp.x * std::cos(rot) - mp.y * std::sin(rot);
		float y = mp.x * std::sin(rot) + mp.y * std::cos(rot);
		mp.x = x;
		mp.y = y;

		Float2 size = c->GetSize();
		Float2 pos = c->GetPosition();
		float zoom = c->GetZoom();
		mp = (mp / wSize);
		// Rotate

		mp = mp * size * zoom;
		mp = mp + pos;
	}

	return mp;
}

void SFML_Renderer::Clear()
{
	Renderer::Clear();
	static sf::Color clearColor(1,1,1);
	m_window.clear(clearColor);
}

void SFML_Renderer::Flush()
{	
	static bool firstRun = true;

	if (firstRun)
	{
		m_recDiscovered.setSize(sf::Vector2f(World::WORLD_SIZE_X, World::WORLD_SIZE_Y));
		m_recUnDiscovered = m_recDiscovered;
		m_texDiscovered.create(World::WORLD_SIZE_X, World::WORLD_SIZE_Y);
		m_texUndiscovered.create(World::WORLD_SIZE_X, World::WORLD_SIZE_Y);
		m_texUndiscovered.clear(sf::Color(0, 0, 0));
		firstRun = false;
	}

	m_texDiscovered.clear(sf::Color(0,0,0,128));

	m_window.setMouseCursorVisible(m_drawMouse);
	m_window.setMouseCursor(m_cursor);
	ImGui::EndFrame();
	Camera * c = Camera::GetActiveCamera();

	if (c)
	{
		sf::View v = m_window.getView();
		Float2 cPos = c->GetPosition();
		Float2 cSiz = c->GetSize();
		float r = c->GetRotation();
		float z = c->GetZoom();
		
		sf::FloatRect fr;
		fr.left = cPos.x;
		fr.top = cPos.y;
		fr.width = cSiz.x;
		fr.height = cSiz.y;
		v.reset(fr);
		v.setRotation(r);
		v.zoom(z);
		v.setCenter(Convert::ConvertToSfmlVec(cPos));
		m_window.setView(v);
	}

	size_t drawQueueSize = m_drawQueue.size();
	size_t drawLineQueueSize = m_drawLineQueue.size();
	size_t backgroundQueueSize = m_backgroundQueue.size();


	// Background
	for (size_t i = 0; i < backgroundQueueSize; i++)
	{
		SFML_Drawable* d = (SFML_Drawable*)m_backgroundQueue[i];

		if (!d)	continue;

		m_window.draw(d->GetMesh());
	}

	// Fog of war
	sf::RenderStates states = sf::RenderStates::Default;
	states.shader = &m_FOWShader;
	states.blendMode = sf::BlendNone;

	for (size_t i = 0; i < drawQueueSize; i++)
	{
		Entity* e = (Entity*)m_drawQueue[i];

		if (!e)	continue;

		/*if (e->GetTeam() != 0)
			continue;*/

		sf::CircleShape c(e->GetViewDistance());
		c.setPosition(e->GetPosition());
		c.setOrigin(sf::Vector2f(c.getRadius(), c.getRadius()));
		m_texDiscovered.draw(c, states);
		m_texUndiscovered.draw(c, states);
	}

	// Real Draw
	for (size_t i = 0; i < drawQueueSize; i++)
	{
		SFML_Drawable* d = (SFML_Drawable*)m_drawQueue[i];

		if (!d)	continue;

		m_window.draw(d->GetMesh());
	}

	m_recDiscovered.setTexture(&m_texDiscovered.getTexture());
	m_recUnDiscovered.setTexture(&m_texUndiscovered.getTexture());
	m_window.draw(m_recDiscovered);
	m_window.draw(m_recUnDiscovered);

	for (size_t i = 0; i < drawLineQueueSize; i++)
	{
		SFML_Line* d = (SFML_Line*)m_drawLineQueue[i];
		if (!d)
			continue;
		m_window.draw(d->GetLine(), 2, sf::PrimitiveType::Lines);
	}
	
	ImGui::SFML::Render(m_window);

	m_window.display();
}

void SFML_Renderer::Update(float dt)
{
	sf::Time t(sf::seconds(dt));
	ImGui::SFML::Update(m_window, t);
}

SFML_Renderer::SFML_Renderer()
{

}

SFML_Renderer::~SFML_Renderer()
{

}

bool SFML_Renderer::IsRunning()
{
	return m_isRunning;
}

void SFML_Renderer::SetWindowTitle(const std::string& title)
{
	m_window.setTitle(title);
}

void SFML_Renderer::SetMouseVisible(bool visible)
{
	m_drawMouse = visible;
	m_window.setMouseCursorVisible(m_drawMouse);
}

void SFML_Renderer::SetMouseCursor(const std::string& name)
{
	SFML_TextureLoader* tl = SFML_TextureLoader::GetInstance();

	if (tl->IsLoaded(name))
	{
		sf::Texture* t = tl->GetTexture(name);
		m_cImage = t->copyToImage();
		
		m_cursor.loadFromPixels(m_cImage.getPixelsPtr(), m_cImage.getSize(), sf::Vector2u(0, 0));
	}
}

void SFML_Renderer::PollEvents()
{
	if (m_isRunning)
	{
		sf::Event event;
		while (m_window.pollEvent(event))
		{
			ImGui::SFML::ProcessEvent(event);
			
			if (event.type == sf::Event::Closed)
			{
				m_isRunning = false;
			}
		}
	}
}

void SFML_Renderer::Exit()
{
	m_isRunning = false;

	_spinlock(m_eventThreadRunning);

	m_window.close();
}
#include <sstream>
#include <SFML/System.hpp>
#include <iostream>
bool SFML_Renderer::_create(unsigned int resX, unsigned int resY, bool threadedProc, bool windowed)
{
	m_drawMouse = true;
	m_cursor.loadFromSystem(sf::Cursor::Arrow);
	m_res.x = resX;
	m_res.y = resY;
	m_isWindowed = windowed;

	std::ostringstream output;
	sf::err().rdbuf(output.rdbuf());

	if (!m_FOWShader.loadFromFile("src/rendering/shaders/fog_of_war_vert.glsl", "src/rendering/shaders/fog_of_war_frag.glsl"))
	{
		std::string err = output.str();
		std::cout << "error!\n" << err << std::endl;
	}

	if (!threadedProc)
	{
		while (!m_window.isOpen())
		{
			if (windowed)
				m_window.create(sf::VideoMode(resX, resY), "projectRTS");
			else
				m_window.create(sf::VideoMode(resX, resY), "projectRTS", sf::Style::Fullscreen);
		}

		m_isRunning = true;
	}
	else
	{
		_startPollEventsThread();
	}

	_spinlock(m_isRunning);

	ImGui::SFML::Init(m_window);
	m_window.setMouseCursorGrabbed(true);

	//m_window.setMouseCursor(m_cursor);

	return true;
}

bool SFML_Renderer::_release()
{
	m_window.close();
	ImGui::SFML::Shutdown();
	return true;
}

void SFML_Renderer::_startPollEventsThread()
{
	if (!m_eventThreadRunning)
	{
		m_pollEventThread = std::thread(&SFML_Renderer::_pollEvents, this);

		bool wndIsOpen = m_window.isOpen();
		while (!wndIsOpen)
			wndIsOpen = m_window.isOpen();

		m_window.setActive(true);

		_spinlock(m_eventThreadRunning);
		m_window.requestFocus();
		m_isRunning = true;
	}
}

void SFML_Renderer::_terminatePollEventsThread()
{
	m_eventThreadRunning = false;
	if (m_pollEventThread.joinable())
		m_pollEventThread.join();
}

void SFML_Renderer::_pollEvents()
{
	while (!m_window.isOpen())
	{
		if (m_isWindowed)
			m_window.create(sf::VideoMode(m_res.x, m_res.y), "projectRTS");
		else
			m_window.create(sf::VideoMode(m_res.x, m_res.y), "projectRTS", sf::Style::Fullscreen);
	}
	m_window.setActive(false);
	m_eventThreadRunning = true;
	while (m_eventThreadRunning && m_isRunning)
	{
		PollEvents();
	}
	m_eventThreadRunning = false;
}

std::string SFML_Renderer::_getFOWShader()
{
	return "";
}

#pragma optimize( "", off)
void SFML_Renderer::_spinlock(bool& dependency)
{
	while (!dependency);
}
#pragma optimize( "", on)
