#ifndef SMFL_TEXTURE_LOADER_H_
#define SMFL_TEXTURE_LOADER_H_

#include <SFML/Graphics.hpp>
#include <string>
#include <map>

class SFML_TextureLoader
{
public:
	static SFML_TextureLoader* GetInstance();
	
	bool LoadFromFile(const std::string& file, const std::string& name);
	bool Unload(const std::string& name);
	bool IsLoaded(const std::string& name);
	sf::Texture* GetTexture(const std::string& name);
	void UnloadAll();

private:
	SFML_TextureLoader();
	~SFML_TextureLoader();

private:
	std::map<std::string, sf::Texture*> m_textureMap;

};

#endif
