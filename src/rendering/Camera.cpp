#include "Camera.h"
#include "../util/Util.h"
Camera* Camera::ACTIVE_CAMERA = nullptr;

Camera::Camera(const Float2& pos, const Float2& size, float rotation)
{
	m_position = pos;
	m_size = size;
	m_rotation = rotation;
	m_zoom = 1.0f;
}

Camera::~Camera()
{
	if (ACTIVE_CAMERA == this)
		ACTIVE_CAMERA = nullptr;
}

void Camera::SetAsActive()
{
	ACTIVE_CAMERA = this;
}

Camera* Camera::GetActiveCamera()
{
	return ACTIVE_CAMERA;
}

void Camera::SetPosition(const Float2& position)
{
	m_position = position;
}

void Camera::SetPosition(float x, float y)
{
	SetPosition({ x, y });
}

void Camera::SetSize(const Float2& size)
{
	m_size = size;
}

void Camera::SetSize(float x, float y)
{
	SetSize({ x, y });
}

void Camera::SetRotation(float r)
{
	m_rotation = r;
}

void Camera::SetZoom(float factor)
{
	m_zoom = factor;
	m_zoom = std::fmax(m_zoom, 0.1f);
}

void Camera::Move(const Float2& dir, bool relative)
{
	if (!relative)
		SetPosition(m_position + dir);
	else
	{
		float rot = Convert::DegreeseToRadians(m_rotation);
		float x = dir.x * std::cos(rot) - dir.y * std::sin(rot);
		float y = dir.x * std::sin(rot) + dir.y * std::cos(rot);

		SetPosition(m_position + Float2(x, y));
	}
}

void Camera::Move(float x, float y, bool relative)
{
	Move(Float2(x, y), relative);
}

void Camera::IncrementSize(const Float2& inc)
{
	SetSize(m_size + inc);
}

void Camera::IncrementSize(float x, float y)
{
	SetSize(m_size + Float2(x, y));
}

void Camera::Rotate(float r)
{
	m_rotation += r;
}

void Camera::IncrementZoom(float factor)
{
	m_zoom += factor;
	m_zoom = std::fmax(m_zoom, 0.1f);
}

void Camera::MultiplyZoom(float factor)
{
	m_zoom *= factor;
	m_zoom = std::fmax(m_zoom, 0.1f);
}

const Float2& Camera::GetPosition() const
{
	return m_position;
}

const Float2& Camera::GetSize() const
{
	return m_size;
}

float Camera::GetZoom() const
{
	return m_zoom;
}

float Camera::GetRotation() const
{
	return m_rotation;
}
