#include "SFMLTextureLoader.h"

SFML_TextureLoader* SFML_TextureLoader::GetInstance()
{
	static SFML_TextureLoader tl;
	return &tl;
}

bool SFML_TextureLoader::LoadFromFile(const std::string& file, const std::string& name)
{
	bool retVal = false;

	if (IsLoaded(name))
		return retVal;

	sf::Texture* t = new sf::Texture();

	m_textureMap.insert_or_assign(name, t);

	if (!(retVal = t->loadFromFile(file)))
	{
		delete m_textureMap[name];
		m_textureMap.erase(name);
	}

	return retVal;
}

bool SFML_TextureLoader::Unload(const std::string& name)
{
	if (!IsLoaded(name))
		return false;

	delete m_textureMap[name];
	m_textureMap.erase(name);

	return true;
}

bool SFML_TextureLoader::IsLoaded(const std::string& name)
{
	return m_textureMap.find(name) != m_textureMap.end();
}

sf::Texture* SFML_TextureLoader::GetTexture(const std::string& name)
{
	return m_textureMap[name];
}

void SFML_TextureLoader::UnloadAll()
{
	for (auto& t : m_textureMap)
		delete t.second;

	m_textureMap.clear();
}

SFML_TextureLoader::SFML_TextureLoader()
{
}

SFML_TextureLoader::~SFML_TextureLoader()
{
	for (auto& t : m_textureMap)
		delete t.second;

	m_textureMap.clear();
}
