#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "../util/Structs.h"

class Camera
{
public:
	Camera(const Float2& pos = Float2(0,0), 
		const Float2& size = Float2(1280, 720),
		float rotation = 0);
	~Camera();


	void SetAsActive();
	static Camera* GetActiveCamera();
	
	void SetPosition(const Float2& position);
	void SetPosition(float x, float y);

	void SetSize(const Float2& size);
	void SetSize(float x, float y);

	void SetRotation(float r);
	void SetZoom(float factor);

	void Move(const Float2& dir, bool relative = false);
	void Move(float x, float y, bool relative = false);
	void IncrementSize(const Float2& inc);
	void IncrementSize(float x, float y);
	void Rotate(float r);
	void IncrementZoom(float factor);
	void MultiplyZoom(float factor);

	const Float2& GetPosition() const;
	const Float2& GetSize() const;
	float GetRotation() const;
	float GetZoom() const;

private:
	Float2 m_position;
	Float2 m_size;
	float m_rotation;
	float m_zoom;

	static Camera* ACTIVE_CAMERA;
};


#endif