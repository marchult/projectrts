#ifndef _SFMLRENDERER_H_
#define _SFMLRENDERER_H_

#include <imgui.h>
#include <imgui-SFML.h>

#include "Renderer.h"
#include <SFML/Graphics.hpp>
#include <thread>

class SFML_Renderer : public Renderer
{
	friend Renderer;
public:

	Float2 GetMousePosition() override;
	Float2 GetMouseWorldPosition() override;
	void Clear() override;
	void Flush() override;
	void Update(float dt) override;
	bool IsRunning() override;
	void SetWindowTitle(const std::string& title) override;
	void SetMouseVisible(bool visible) override;
	void SetMouseCursor(const std::string& name) override;
	void PollEvents() override;
	void Exit() override;

private:
	SFML_Renderer();
	~SFML_Renderer();

private:
	bool _create(unsigned int resX, unsigned int resY, bool threadedProc, bool windowed) override;
	bool _release() override;
	void _startPollEventsThread() override;
	void _terminatePollEventsThread() override;

	void _pollEvents();
	void _spinlock(bool& dependency);

	std::string _getFOWShader();

private:
	sf::RenderWindow m_window;
	sf::Vector2u m_res;
	bool m_isWindowed;
	bool m_isRunning;

	bool m_drawMouse;
	sf::Cursor m_cursor;
	sf::Image m_cImage;

	std::thread m_pollEventThread;
	bool m_eventThreadRunning = false;

	sf::RenderTexture m_texUndiscovered;
	sf::RenderTexture m_texDiscovered;
	sf::Shader m_FOWShader;

	sf::RectangleShape m_recUnDiscovered;
	sf::RectangleShape m_recDiscovered;

};


#endif