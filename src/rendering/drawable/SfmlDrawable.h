#ifndef _SFMLDRAWABLE_H_
#define _SFMLDRAWABLE_H_

#include "Drawable.h"
#include <SFML/Graphics.hpp>

class SFML_Drawable : public Drawable
{
public:
	SFML_Drawable();
	~SFML_Drawable();

	virtual void SetPosition(const sf::Vector2f & position);
	virtual void SetPosition(float x, float y);
	virtual void SetSize(const sf::Vector2f& size);
	virtual void SetSize(float x, float y);
	virtual void SetRotation(float r);
	
	virtual void Move(const sf::Vector2f& direction);
	virtual void Move(float x, float y);
	virtual void Scale(const sf::Vector2f& scale);
	virtual void Scale(float x, float y);
	virtual void Rotate(float r);
	
	void SetColor(const sf::Color & color);
	void SetColor(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a = 255);
	void SetOutlineColor(const sf::Color& color);
	void SetOutlineColor(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a = 255);
	void SetOutlineThickness(float t);
	void SetOrigin(const sf::Vector2f& origin);

	sf::Vector2f GetPosition() const;
	sf::Vector2f GetSize() const;
	float GetRotation() const;
	sf::Color GetColor() const;
	sf::Color GetOutlineColor() const;
	float GetOutlineThickness() const;

	const sf::RectangleShape& GetMesh() const;
	
	void SetTexture(sf::Texture* t);

	void Draw() override;
	void DrawAsBackground() override;
private:
	void _updateOrigin();

private:
	sf::RectangleShape m_mesh;
	
};

#endif // !_SFMLDRAWABLE_H_

