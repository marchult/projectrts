#include "SfmlDrawable.h"
#include "../Renderer.h"

SFML_Drawable::SFML_Drawable()
{
	
}

SFML_Drawable::~SFML_Drawable()
{
}

void SFML_Drawable::SetPosition(const sf::Vector2f& position)
{
	m_mesh.setPosition(position);
}

void SFML_Drawable::SetPosition(float x, float y)
{
	SetPosition( {x, y} );
}

void SFML_Drawable::SetSize(const sf::Vector2f& size)
{
	m_mesh.setSize(size);
	_updateOrigin();
}

void SFML_Drawable::SetSize(float x, float y)
{
	SetSize({ x, y });
}

void SFML_Drawable::SetRotation(float r)
{
	m_mesh.setRotation(r);
}

void SFML_Drawable::Move(const sf::Vector2f& direction)
{
	m_mesh.move(direction);
}

void SFML_Drawable::Move(float x, float y)
{
	Move({ x, y });
}

void SFML_Drawable::Scale(const sf::Vector2f& scale)
{
	m_mesh.setSize(m_mesh.getSize() + scale);
	_updateOrigin();
}

void SFML_Drawable::Scale(float x, float y)
{
	Scale({ x, y });
}

void SFML_Drawable::Rotate(float r)
{
	m_mesh.rotate(r);
}

void SFML_Drawable::SetColor(const sf::Color& color)
{
	m_mesh.setFillColor(color);
}

void SFML_Drawable::SetColor(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a)
{
	SetColor({ r, g, b, a });
}

void SFML_Drawable::SetOutlineColor(const sf::Color& color)
{
	m_mesh.setOutlineColor(color);
}

void SFML_Drawable::SetOutlineColor(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a)
{
	SetOutlineColor({ r, g, b, a });
}

void SFML_Drawable::SetOutlineThickness(float t)
{
	m_mesh.setOutlineThickness(t);
}

void SFML_Drawable::SetOrigin(const sf::Vector2f& origin)
{
	m_mesh.setOrigin(origin);
}

sf::Vector2f SFML_Drawable::GetPosition() const
{
	return m_mesh.getPosition();
}

sf::Vector2f SFML_Drawable::GetSize() const
{
	return m_mesh.getSize();
}

float SFML_Drawable::GetRotation() const
{
	return m_mesh.getRotation();
}

sf::Color SFML_Drawable::GetColor() const
{
	return m_mesh.getFillColor();
	
}

sf::Color SFML_Drawable::GetOutlineColor() const
{
	return m_mesh.getOutlineColor();
}

float SFML_Drawable::GetOutlineThickness() const
{
	return m_mesh.getOutlineThickness();
}

const sf::RectangleShape& SFML_Drawable::GetMesh() const
{
	return m_mesh;
}

void SFML_Drawable::SetTexture(sf::Texture* t)
{
	m_mesh.setTexture(t);
}

void SFML_Drawable::Draw()
{
	Renderer::GetInstance()->Draw(this);
}

void SFML_Drawable::DrawAsBackground()
{
	Renderer::GetInstance()->DrawAsBackground(this);
}

void SFML_Drawable::_updateOrigin()
{
	sf::Vector2f s = m_mesh.getSize();
	s *= 0.5f;
	m_mesh.setOrigin(s);
}
