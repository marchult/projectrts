#include "SfmlLine.h"
#include "../Renderer.h"

SFML_Line::SFML_Line()
{
	m_line[0].color = sf::Color::White;
	m_line[0].position = sf::Vector2f(0.0f, 0.0f);
	m_line[1].color = sf::Color::White;
	m_line[1].position = sf::Vector2f(0.0f, 0.0f);
}

SFML_Line::SFML_Line(const sf::Vector2f& p1, const sf::Vector2f& p2)
{
	m_line[0].color = sf::Color::White;
	m_line[0].position = p1;
	m_line[1].color = sf::Color::White;
	m_line[1].position = p2;
}

SFML_Line::SFML_Line(float x1, float y1, float x2, float y2)
{
	m_line[0].color = sf::Color::White;
	m_line[0].position = { x1, y1 };
	m_line[1].color = sf::Color::White;
	m_line[1].position = { x2, y2 };
}

SFML_Line::~SFML_Line()
{
}

void SFML_Line::SetPosition(const sf::Vector2f& p1, const sf::Vector2f& p2)
{
	m_line[0].position = p1;
	m_line[1].position = p2;
}

void SFML_Line::SetPosition(float x1, float y1, float x2, float y2)
{
	SetPosition({ x1, y1 }, { x2, y2 });
}

void SFML_Line::SetPosition1(const sf::Vector2f& p1)
{
	m_line[0].position = p1;
}

void SFML_Line::SetPosition1(float x1, float y1)
{
	SetPosition1({ x1, y1 });
}

void SFML_Line::SetPosition2(const sf::Vector2f& p2)
{
	m_line[1].position = p2;
}

void SFML_Line::SetPosition2(float x2, float y2)
{
	SetPosition1({ x2, y2 });
}

void SFML_Line::SetColor(const sf::Color& color)
{
	m_line[0].color = color;
	m_line[1].color = color;
}

void SFML_Line::SetColor(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a)
{
	SetColor({r, g, b, a});
}

sf::Vector2f SFML_Line::GetP1() const
{
	return m_line[0].position;
}

sf::Vector2f SFML_Line::GetP2() const
{
	return m_line[1].position;
}

sf::Color SFML_Line::GetColor() const
{
	return m_line[0].color;
}

sf::Vertex* SFML_Line::GetLine()
{
	return m_line;
}

void SFML_Line::Draw()
{
	Renderer::GetInstance()->DrawLine(this);
}
