#ifndef _SFML_LINE_H_
#define _SFML_LINE_H_

#include "Drawable.h"
#include <SFML/Graphics.hpp>

class SFML_Line : public Drawable
{
public:
	SFML_Line();
	SFML_Line(const sf::Vector2f& p1, const sf::Vector2f& p2);
	SFML_Line(float x1, float y1, float x2, float y2);
	~SFML_Line();

	void SetPosition(const sf::Vector2f& p1, const sf::Vector2f& p2);
	void SetPosition(float x1, float y1, float x2, float y2);
	
	void SetPosition1(const sf::Vector2f& p1);
	void SetPosition1(float x1, float y1);
	void SetPosition2(const sf::Vector2f& p2);
	void SetPosition2(float x2, float y2);

	void SetColor(const sf::Color& color);
	void SetColor(sf::Uint8 r, sf::Uint8 g, sf::Uint8 b, sf::Uint8 a = 255);
	
	sf::Vector2f GetP1() const;
	sf::Vector2f GetP2() const;
	
	sf::Color GetColor() const;
	
	sf::Vertex* GetLine();

	void Draw() override;
	void DrawAsBackground() override {}
private:
	sf::Vertex m_line[2];

};


#endif
