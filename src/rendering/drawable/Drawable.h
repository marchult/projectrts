#ifndef _DRAWABLE_H_
#define _DRAWABLE_H_

class Drawable
{
public:
	Drawable();
	
	virtual ~Drawable();
	virtual void Draw() = 0;
	virtual void DrawAsBackground() = 0;
};


#endif // !_DRAWABLE_H_

