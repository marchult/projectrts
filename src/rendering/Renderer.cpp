#include "Renderer.h"
#include "SFMLRenderer.h"

bool Renderer::s_isInitialized = false;
Renderer* Renderer::s_renderer = nullptr;

void Renderer::Initialize(RENDERER_TYPE type, unsigned int resX, unsigned int resY, bool threadedProc, bool windowed)
{
	if (!s_isInitialized)
	{
		s_isInitialized = true;
		switch (type)
		{
		case RENDERER_TYPE::SFML:
		default:
			s_renderer = new SFML_Renderer;
			s_renderer->_create(resX, resY, threadedProc, windowed);
			break;
		}
	}
}

Renderer* Renderer::GetInstance()
{
	return s_renderer;
}

void Renderer::Draw(Drawable* drawable)
{
	m_drawQueue.push_back(drawable);
}

void Renderer::DrawLine(Drawable* drawable)
{
	m_drawLineQueue.push_back(drawable);
}

void Renderer::DrawAsBackground(Drawable* drawable)
{
	m_backgroundQueue.push_back(drawable);
}

void Renderer::Clear()
{
	m_drawQueue.clear();
	m_drawLineQueue.clear();
	m_backgroundQueue.clear();
}

void Renderer::Release()
{
	if (s_isInitialized)
	{
		s_renderer->m_drawQueue.clear();
		s_renderer->m_drawLineQueue.clear();
		s_renderer->m_backgroundQueue.clear();
		s_renderer->_terminatePollEventsThread();
		s_renderer->_release();
		delete s_renderer;
		s_renderer = nullptr;
		s_isInitialized = false;
	}
}

Renderer::Renderer()
{

}

Renderer::~Renderer()
{
	
}
