void main()
{
	vec4 vertex = gl_Vertex;
	vertex = gl_ModelViewMatrix * vertex;
	gl_Position = gl_ProjectionMatrix * vertex;
	gl_Position.y = gl_Position.y * -1.0f;
}