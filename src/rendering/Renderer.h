#ifndef _RENDERER_H_
#define _RENDERER_H_
#include <vector>
#include <string>
#include "../util/Structs.h"
#include "Camera.h"

class Drawable;

class Renderer
{
public:
	enum RENDERER_TYPE
	{
		SFML
	};

public:
	static void Initialize(RENDERER_TYPE type, unsigned int resX, unsigned int resY, bool threadedProc = true, bool windowed = true);
	static Renderer* GetInstance();

	virtual Float2 GetMousePosition() = 0;
	virtual Float2 GetMouseWorldPosition() = 0;
	virtual void Draw(Drawable* drawable);
	virtual void DrawLine(Drawable* drawable);
	virtual void DrawAsBackground(Drawable* drawable);
	virtual void Flush() = 0;
	virtual void Clear();
	static void Release();
	virtual void Update(float dt) = 0;
	virtual bool IsRunning() = 0;
	virtual void SetWindowTitle(const std::string& title) = 0;
	virtual void SetMouseVisible(bool visible) = 0;
	virtual void SetMouseCursor(const std::string & name) = 0;

	/* 
		Only use PollEvents() if Renderer is initialised with threadedProc = false
		Use this between Clear() and Update().
	*/
	virtual void PollEvents() = 0;
	virtual void Exit() = 0;

protected:
	Renderer();
	virtual ~Renderer();

	virtual bool _create(unsigned int resX, unsigned int resY, bool threadedProc, bool windowed) = 0;
	virtual bool _release() = 0;

	virtual void _startPollEventsThread() = 0;
	virtual void _terminatePollEventsThread() = 0;

private:
	static bool s_isInitialized;
	static Renderer* s_renderer;
protected:
	std::vector<Drawable*> m_drawQueue;
	std::vector<Drawable*> m_drawLineQueue;
	std::vector<Drawable*> m_backgroundQueue;
};


#endif // !_RENDERER_H_