#ifndef _TERRAIN_CREATOR_H_
#define _TERRAIN_CREATOR_H_

#include <SFML/Graphics.hpp>
#include <string>
#include "Region.h"

class TerrainCreator
{
public:
	enum Terrain_Type
	{
		Random = 0,
		Islands,
		Lakes,
		Plain,
		Mountain
	};
	static const sf::Color WATER;
	static const sf::Color SAND;
	static const sf::Color GRASS;
	static const sf::Color ROCK;
	static const sf::Color SNOW;


	TerrainCreator();
	void LoadFromFile(const std::string & path);
	void Create(unsigned int mapSize, Terrain_Type type);
	const std::vector<Region::Line> & GetEdges() const;
	Region::Region_Type GetRegionAt(unsigned int x, unsigned int y) const;
	bool IsType(unsigned int x, unsigned int y, Region::Region_Type t) const;

	sf::Texture * GetTexture();
private:
	struct TerrainColors
	{
		std::vector<sf::Color> Colors;
		std::vector<int> HeightPass;
	};
	void _createDataFromHeightmapSimple(const std::vector<float>& input, unsigned int size, const TerrainColors& col);


private:
	sf::Texture m_texture;
	Region m_regions;

};

#endif
