#include "Region.h"
#include <map>
#include <iostream>

sf::Image Region::m_dbg;

Region::Region()
{
}

void Region::Create(unsigned int x, unsigned int y)
{
	m_width = x;
	m_height = y;
	m_region.resize(x * y);
}

void Region::Set(unsigned int x, unsigned int y, const Region::PixelRegion & pr)
{
	m_region[x + y * m_width] = pr;
}

const Region::PixelRegion & Region::At(unsigned int x, unsigned int y) const
{
	return m_region[x + y * m_width];
}

bool Region::IsType(unsigned int x, unsigned int y, Region_Type t) const
{
	return m_region[x + y * m_width].t == t;
}

unsigned int Region::GetWidth() const
{
	return m_width;
}

unsigned int Region::GetHeight() const
{
	return m_height;
}

#define uint unsigned int

void Region::EdgeDetect()
{
	std::vector<bool> water(m_region.size());
	std::vector<bool> block(m_region.size());

	std::vector<uint> remWater;
	std::vector<uint> remBlock;


	for (uint y = 1; y < m_height - 1; y++)
	{
		for (uint x = 1; x < m_width - 1; x++)
		{
			uint index = x + y * m_width;
			water[index] = m_region[index].t == Water;
			block[index] = m_region[index].t == Blocked;
		}
	}

	std::vector<bool> waterDbg = water;
	std::vector<bool> blockDbg = block;

	for (uint y = 1; y < m_height - 1; y++)
	{
		for (uint x = 1; x < m_width - 1; x++)
		{
			uint r = (x + 1) + (y + 0) * m_width;
			uint l = (x - 1) + (y + 0) * m_width;
			uint u = (x + 0) + (y - 1) * m_width;
			uint d = (x - 0) + (y + 1) * m_width;
			
			if (waterDbg[r] && waterDbg[l] && waterDbg[u] && waterDbg[d])
				remWater.push_back(x + y * m_width);
			if (blockDbg[r] && blockDbg[l] && blockDbg[u] && blockDbg[d])
				remBlock.push_back(x + y * m_width);
		}
	}
	for (auto& i : remWater)
		waterDbg[i] = false;
	for (auto& i : remBlock)
		blockDbg[i] = false;

	m_dbg.create(m_width, m_height, sf::Color::Transparent);

	/*for (uint y = 0; y < m_height; y++)
	{
		for (uint x = 0; x < m_width; x++)
		{
			uint index = x + y * m_width;
			
			if (waterDbg[index] || blockDbg[index])
				m_dbg.setPixel(x, y, sf::Color::White);
		}
	}*/

	std::vector<std::vector<uint>> blockEdges = _traverse2(block);
	{
		std::vector<std::vector<uint>> waterEdges = _traverse2(water);
		blockEdges.insert(blockEdges.end(), waterEdges.begin(), waterEdges.end());
	}

	std::cout << "Creating Lines for edges...\n";
	for (auto& ev : blockEdges)
	{
		uint size = ev.size();
		if (!size)
			continue;
		std::vector<uint> lines;

		lines.push_back(ev.front());

		for (uint i = 1; i < size - 1; i++)
		{
			if (i % 10 == 0)
				lines.push_back(ev[i]);
		}

		lines.push_back(ev.back());

		size = lines.size();

		for (uint i = 0; i < size - 1; i++)
		{
			Line l;
			if (i == 0)
				l.f = 0;
			else if (i == (size - 2))
				l.f = 2;
			uint x1 = lines[i] % m_width;
			uint y1 = lines[i] / m_width;
			uint x2 = lines[i + 1] % m_width;
			uint y2 = lines[i + 1] / m_width;

			l.a.x = (float)x1 + 0.5f;
			l.a.y = (float)y1 + 0.5f;
			l.b.x = (float)x2 + 0.5f;
			l.b.y = (float)y2 + 0.5f;

			m_edges.push_back(l);
		}
	}

	std::cout << "Number of edges created: " << m_edges.size() << "\n";

}

const std::vector<Region::Line>& Region::GetEdges() const
{
	return m_edges;
}

std::vector<std::vector<unsigned int>> Region::_traverse(const std::vector<bool>& map) const
{
	std::vector<std::vector<unsigned int>> edges;
	std::vector<bool> closedList(map.size());

	for (uint y = 1; y < m_height - 1; y++)
	{
		for (uint x = 1; x < m_width - 1; x++)
		{
			uint index = x + y * m_width;
			if (map[index] && !closedList[index])
			{
				std::vector<unsigned int> trav;

				Float2 goal;
				goal.x = x;
				goal.y = y;
				bool canTraverse = true;

				uint current = index;
				trav.push_back(current);
				while (canTraverse)
				{
					closedList[current] = true;
					std::vector<uint> neighbours;

					uint cx = current % m_width;
					uint cy = current / m_width;

					uint _ri = (cx + 1) + (cy + 0) * m_width;
					uint _do = (cx + 0) + (cy + 1) * m_width;
					uint _le = (cx - 1) + (cy + 0) * m_width;
					uint _up = (cx + 0) + (cy - 1) * m_width;
					uint _dr = (cx + 1) + (cy + 1) * m_width;
					uint _dl = (cx - 1) + (cy + 1) * m_width;
					uint _ur = (cx + 1) + (cy - 1) * m_width;
					uint _ul = (cx - 1) + (cy - 1) * m_width;

					if (map[ _ri] && !closedList[ _ri])
						neighbours.push_back(_ri);
					if (map[ _do] && !closedList[ _do])
						neighbours.push_back(_do);
					if (map[ _le] && !closedList[ _le])
						neighbours.push_back(_le);
					if (map[ _up] && !closedList[ _up])
						neighbours.push_back(_up);
					if (map[ _dr] && !closedList[ _dr])
						neighbours.push_back(_dr);
					if (map[ _dl] && !closedList[ _dl])
						neighbours.push_back(_dl);
					if (map[ _ur] && !closedList[ _ur])
						neighbours.push_back(_ur);
					if (map[ _ul] && !closedList[ _ul])
						neighbours.push_back(_ul);

					if (neighbours.empty())
						canTraverse = false;
					else
					{
						uint next;
						float _cost = FLT_MAX;
						for (auto& n : neighbours)
						{
							uint _x = n % m_width;
							uint _y = n / m_height;

							Float2 now;
							now.x = _x;
							now.y = _y;

							Float2 delta = (goal - now).abs();
							float tCost = std::max(delta.x, delta.y) + (-0.414f) * std::min(delta.x, delta.y) + trav.size();
							if (tCost < _cost)
							{
								next = _x + _y * m_width;
								_cost = tCost;
							}
						}
						trav.push_back(next);
						if (next == current)
						{
							canTraverse = false;
							trav.push_back(trav.front());
						}
						else
						{
							current = next;
						}
					}
				}

				if (trav.size() > 1)
				{
					uint s = trav.front();
					uint d = trav.back();
					Float2 ss;
					Float2 dd;
					ss.x = s % m_width;
					ss.y = s / m_width;
					dd.x = d % m_width;
					dd.y = d / m_width;

					if ((dd - ss).length() < 5)
						trav.push_back(trav.front());

					edges.push_back(trav);
				}
			}
		}
	}

	return edges;
}

std::vector<std::vector<unsigned int>> Region::_traverse2(const std::vector<bool>& map) const
{
	std::cout << "Defining Terrain \"Object\"...\n";
	std::vector<std::vector<unsigned int>> edges;
	std::vector<bool> closedMap(map.size());

	std::vector<Object> objects;
	for (uint y = 0; y < m_height; y++)
	{
		for (uint x = 0; x < m_width; x++)
		{
			uint index = x + y * m_width;
			if (!closedMap[index] && map[index])
			{
				Object o = _getObjectAt(x, y, map, closedMap);
				if (!o.objectQuad.empty())
				{
					objects.push_back(o);
				}
			}
		}
	}

	std::cout << "Objects found: " << objects.size() << "\n";
	std::cout << "Detecting and traversing edges for objects...\n";
	for (auto& o : objects)
	{
		_edgeDetectForObject(o);
		auto newEdges = _getEdgesForObject(o);
		edges.insert(edges.end(), newEdges.begin(), newEdges.end());
	}

	return edges;
}

Region::Object Region::_getObjectAt(unsigned x, unsigned y, const std::vector<bool>& map, std::vector<bool>& closedObjectMap) const
{
	POINT size, pos;
	std::vector<POINT> object;
	_subTraverse(x, y, map, closedObjectMap, object);
	std::sort(object.begin(), object.end());

	uint left = m_width, top = m_height, right = 0, bot = 0;

	for (auto& p : object)
	{
		if (p.x < left)
			left = p.x;
		if (p.x > right)
			right = p.x;
		if (p.y < top)
			top = p.y;
		if (p.y > bot)
			bot = p.y;
	}

	pos.x = left;
	pos.y = top;
	size.x = right - left + 1;
	size.y = bot -   top  + 1;

	uint centerX = 0;
	uint centerY = 0;

	for (auto& p : object)
	{
		p.x -= left;
		p.y -= top;
		centerX += p.x;
		centerY += p.y;
	}

	centerX /= object.size();
	centerY /= object.size();

	std::map<uint, bool> objectMap;

	for (auto& p : object)
	{
		objectMap.insert_or_assign(p.x + p.y * size.x, true);
	}

	object.clear();
	object.resize(size.x * size.y);

	for (uint _y = 0; _y < size.y; _y++)
	{
		for (uint _x = 0; _x < size.x; _x++)
		{
			uint index = _x + _y * size.x;
			object[index].x = _x;
			object[index].y = _y;
			object[index].isObjectPixel = false;

			if (objectMap.find(index) != objectMap.end())
				object[index].isObjectPixel = true;

		}
	}
	
	Object o;
	o.pos = pos;
	o.size = size;
	o.objectQuad = object;
	o.center.x = centerX;
	o.center.y = centerY;

	return o;
}

void Region::_subTraverse(unsigned x, unsigned y, const std::vector<bool>& map, std::vector<bool>& closedObjectMap, std::vector<POINT>& object) const
{
	uint index = x + y * m_width;
	if (map[index] && !closedObjectMap[index])
	{
		closedObjectMap[index] = true;
		POINT p;
		p.x = x;
		p.y = y;
		p.isObjectPixel = true;
		object.push_back(p);
		std::vector<uint> trav;

		trav.push_back((x + 1) + (y + 0) * m_width);
		trav.push_back((x - 1) + (y + 0) * m_width);
		trav.push_back((x + 0) + (y + 1) * m_width);
		trav.push_back((x + 0) + (y - 1) * m_width);
		trav.push_back((x + 1) + (y + 1) * m_width);
		trav.push_back((x - 1) + (y + 1) * m_width);
		trav.push_back((x + 1) + (y - 1) * m_width);
		trav.push_back((x - 1) + (y - 1) * m_width);

		while (!trav.empty())
		{
			uint _x = trav.back() % m_width;
			uint _y = trav.back() / m_width;
			uint _index = _x + _y * m_width;
			trav.pop_back();

			if (map[_index] && !closedObjectMap[_index])
			{
				closedObjectMap[_index] = true;
				p.x = _x;
				p.y = _y;
				p.isObjectPixel = true;
				object.push_back(p);

				trav.push_back((_x + 1) + (_y + 0) * m_width);
				trav.push_back((_x - 1) + (_y + 0) * m_width);
				trav.push_back((_x + 0) + (_y + 1) * m_width);
				trav.push_back((_x + 0) + (_y - 1) * m_width);
				trav.push_back((_x + 1) + (_y + 1) * m_width);
				trav.push_back((_x - 1) + (_y + 1) * m_width);
				trav.push_back((_x + 1) + (_y - 1) * m_width);
				trav.push_back((_x - 1) + (_y - 1) * m_width);
			}
		}
	}
}

void Region::_edgeDetectForObject(Object& o) const
{
	std::vector<uint> markedForDel;
	for (uint y = 1; y < (int)o.size.y - 1; y++)
	{
		for (uint x = 1; x < (int)o.size.x - 1; x++)
		{
			uint index = x + y * o.size.x;
			if (o.objectQuad[index].isObjectPixel)
			{
				uint l = (x - 1) + y * o.size.x,
					 r = (x + 1) + y * o.size.x,
					 u = x + (y - 1) * o.size.x,
					 d = x + (y + 1) * o.size.x;

				if (o.objectQuad[l].isObjectPixel &&
					o.objectQuad[r].isObjectPixel &&
					o.objectQuad[u].isObjectPixel &&
					o.objectQuad[d].isObjectPixel)
				{
					markedForDel.push_back(index);
				}
			}
		}
	}

	for (auto& d : markedForDel)
		o.objectQuad[d].isObjectPixel = false;
}

std::vector<std::vector<unsigned int>> Region::_getEdgesForObject(const Object& o) const
{
	std::vector<std::vector<unsigned int>> edges;

	std::vector<bool> closedList(o.objectQuad.size());

	uint nrOfEdgePixels = 0;
	for (auto p : o.objectQuad)
	{
		nrOfEdgePixels += p.isObjectPixel;

		if (p.isObjectPixel)
			m_dbg.setPixel(o.pos.x + p.x, o.pos.y + p.y, sf::Color::White);

	}

	for (uint y = 0; y < o.size.y; y++)
	{
		for (uint x = 0; x < o.size.x; x++)
		{
			uint index = x + y * o.size.x;
			if (o.objectQuad[index].isObjectPixel && !closedList[index])
			{
				bool first = true;
				std::vector<unsigned int> trav;

				Float2 goal;
				goal.x = x;
				goal.y = y;
				bool canTraverse = true;

				uint current = index;
				uint prev = index;
				trav.push_back(current);
				while (canTraverse)
				{
					long cx = current % o.size.x;
					long cy = current / o.size.x;

					long dirs[8];
					uint nrOfDirs = 0;

					if (cx + 1 < o.size.x)	// Right
						dirs[nrOfDirs++] = (cx + 1) + (cy + 0) * o.size.x;
					if (cy + 1 < o.size.y)	// Down
						dirs[nrOfDirs++] = (cx + 0) + (cy + 1) * o.size.x;
					if (cx - 1 >= 0)		// Left
						dirs[nrOfDirs++] = (cx - 1) + (cy + 0) * o.size.x;
					if (cy - 1 >= 0)		// Up
						dirs[nrOfDirs++] = (cx + 0) + (cy - 1) * o.size.x;

					if (cx + 1 < o.size.x && cy + 1 < o.size.y) // Right-Down
						dirs[nrOfDirs++] = (cx + 1) + (cy + 1) * o.size.x;
					if (cx - 1 >= 0 && cy + 1 < o.size.y)		// Left-Down
						dirs[nrOfDirs++] = (cx - 1) + (cy + 1) * o.size.x;
					if (cx + 1 < o.size.x && cy - 1 >= 0)		// Right-Up
						dirs[nrOfDirs++] = (cx + 1) + (cy - 1) * o.size.x;
					if (cx - 1 >= 0 && cy - 1 >= 0)				// Left-Up
						dirs[nrOfDirs++] = (cx - 1) + (cy - 1) * o.size.x;

					std::vector<uint> neighbours;
					for (uint i = 0; i < nrOfDirs; i++)
					{
						if (o.objectQuad[dirs[i]].isObjectPixel && !closedList[dirs[i]])
							neighbours.push_back(dirs[i]);
					}
					
					if (neighbours.empty())
					{
						canTraverse = false;
						for (uint i = 0; i < nrOfDirs; i++)
						{
							if (o.objectQuad[dirs[i]].isObjectPixel && dirs[i] != prev)
								neighbours.push_back(dirs[i]);
						}
					}

					if (!neighbours.empty())
					{
						long next = -1;
						float _cost = 0;
						if (trav.size() > (nrOfEdgePixels / 2 + 1))
							_cost = FLT_MAX;

						for (auto& n : neighbours)
						{
							if (!canTraverse && trav.size() > 1)
							{
								if (n == index)
								{
									next = index;
									break;
								}
							}

							uint _x = n % o.size.x;
							uint _y = n / o.size.x;

							Float2 now;
							now.x = _x;
							now.y = _y;
							Float2 center;
							center.x = o.center.x;
							center.y = o.center.y;
							Float2 delta = (center - now).abs();
							float tCost = std::max(delta.x, delta.y) + (-0.414f) * std::min(delta.x, delta.y);
							delta = (now - goal).abs();
							tCost = std::max(delta.x, delta.y) + (-0.414f) * std::min(delta.x, delta.y);
							
							bool isTrue = tCost > _cost;
							if (trav.size() > (nrOfEdgePixels / 2 + 1))
								isTrue = tCost < _cost;

							if (isTrue)
							{
								next = _x + _y * o.size.x;
								_cost = tCost;
							}
						}
						if (next != -1)
						{
							trav.push_back(next);
							if (next == index)
							{
								canTraverse = false;
							}
							else
							{
								prev = current;
								current = next;
								closedList[current] = true;
							}
						}
						else
							canTraverse = false;
					}
				}

				if (trav.size() > 1)
				{
					edges.push_back(trav);
				}
			}
		}
	}

	
	// Transform back to world space
	for (auto & e : edges)
	{
		for (auto& ee : e)
		{
			uint _x = ee % o.size.x;
			uint _y = ee / o.size.x;
			_x += o.pos.x;
			_y += o.pos.y;
			ee = _x + _y * m_width;
		}
	}

	return edges;
}
