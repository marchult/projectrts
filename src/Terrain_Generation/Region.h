#ifndef _REGION_H_
#define _REGION_H_

#include "../util/Structs.h"
#include <vector>
#include <SFML/Graphics.hpp>

class Region
{
public:
	enum Region_Type
	{
		Land,
		Blocked,
		Water,
	};

	struct PixelRegion
	{
		Region_Type t = Land;
		float ang = 1.0;
	};

	struct Line
	{
		Float2 a, b;
		int f = 1;
	};

	static sf::Image m_dbg;

public:
	Region();

	void Create(unsigned int x, unsigned int y);
	void Set(unsigned int x, unsigned int y, const Region::PixelRegion& pr);
	const PixelRegion & At(unsigned int x, unsigned int y) const;
	bool IsType(unsigned int x, unsigned int y, Region_Type t) const;
	unsigned int GetWidth() const;
	unsigned int GetHeight() const;
	void EdgeDetect();

	const std::vector<Line>& GetEdges() const;
	void CleanEdges() { m_edges.clear(); }

private:
	std::vector<PixelRegion> m_region;
	unsigned int m_width;
	unsigned int m_height;

	std::vector<Line> m_edges;


	std::vector<std::vector<unsigned int>> _traverse(const std::vector<bool>& map) const;

	struct POINT
	{
		unsigned int x, y;
		bool isObjectPixel = false;
		bool operator<(const POINT& other)
		{
			return y < other.y || (x < other.x && y <= other.y);
		}
		bool operator>(const POINT& other)
		{
			return y > other.y || (x > other.x && y >= other.y);
		}
		bool operator==(const POINT& other)
		{
			return y == other.y && x == other.x;
		}

	};

	struct Object
	{
		POINT pos, size;
		POINT center;
		std::vector<POINT> objectQuad;
	};

	std::vector<std::vector<unsigned int>> _traverse2(const std::vector<bool>& map) const;
	Object _getObjectAt(unsigned x, unsigned y, const std::vector<bool>& map, std::vector<bool>& closedObjectMap) const;
	void _subTraverse(unsigned x, unsigned y, const std::vector<bool>& map, std::vector<bool>& closedObjectMap, std::vector<POINT>& object) const;
	void _edgeDetectForObject(Object & o) const;
	std::vector<std::vector<unsigned int>> _getEdgesForObject(const Object& o) const;
};

#endif
