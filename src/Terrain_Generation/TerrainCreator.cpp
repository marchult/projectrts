#include "TerrainCreator.h"
#include "DiamondSquare.h"
#include <iostream>

const sf::Color TerrainCreator::WATER = sf::Color(0,0,255);
const sf::Color TerrainCreator::SAND = sf::Color(255, 255, 0);
const sf::Color TerrainCreator::GRASS = sf::Color(0, 255, 0);
const sf::Color TerrainCreator::ROCK = sf::Color(128,128,128);
const sf::Color TerrainCreator::SNOW = sf::Color(255, 255, 255);

TerrainCreator::TerrainCreator()
{
	
}

void TerrainCreator::LoadFromFile(const std::string& path)
{
	sf::Image input;
	input.loadFromFile(path);

	TerrainColors tc;

	tc.Colors.resize(5);
	tc.HeightPass.resize(5);

	tc.Colors[0] = WATER;
	tc.HeightPass[0] = 50;

	tc.Colors[1] = SAND;
	tc.HeightPass[1] = 75;

	tc.Colors[2] = GRASS;
	tc.HeightPass[2] = 150;

	tc.Colors[3] = ROCK;
	tc.HeightPass[3] = 220;

	tc.Colors[4] = SNOW;
	tc.HeightPass[3] = 256;

	std::vector<float> heightVal(input.getSize().x * input.getSize().y);

	for (int y = 0; y < input.getSize().y; y++)
		for (int x = 0; x < input.getSize().x; x++)
		{
			int index = x + y * input.getSize().x;
			heightVal[index] = input.getPixel(x, y).r;
		}

	_createDataFromHeightmapSimple(heightVal, input.getSize().x, tc);
	m_regions.EdgeDetect();
}

void TerrainCreator::Create(unsigned int mapSize, Terrain_Type type)
{
	std::cout << "Generating Terrain based on Terrain_Type\n";
	DiamondSquare ds;
	TerrainColors tc;
	std::vector<float> heightVals;

	float noise = 1;

	switch (type)
	{
	case TerrainCreator::Islands:
		noise = 2;
		heightVals = ds.CreateDiamondSquare(mapSize, (mapSize - 1) / 8, noise, -3, 6, 2);
		
		tc.Colors.resize(5);
		tc.HeightPass.resize(5);

		tc.Colors[0] = WATER;
		tc.HeightPass[0] = 10;

		tc.Colors[1] = SAND;
		tc.HeightPass[1] = 11;

		tc.Colors[2] = GRASS;
		tc.HeightPass[2] = 150;

		tc.Colors[3] = ROCK;
		tc.HeightPass[3] = 200;

		tc.Colors[4] = SNOW;
		tc.HeightPass[3] = 256;

		break;
	case TerrainCreator::Lakes:
		noise = 7;
		heightVals = ds.CreateDiamondSquare(mapSize, mapSize, noise, -12, 17, 1);

		tc.Colors.resize(5);
		tc.HeightPass.resize(5);

		tc.Colors[0] = WATER;
		tc.HeightPass[0] = 0;

		tc.Colors[1] = SAND;
		tc.HeightPass[1] = 11;

		tc.Colors[2] = GRASS;
		tc.HeightPass[2] = 100;

		tc.Colors[3] = ROCK;
		tc.HeightPass[3] = 200;

		tc.Colors[4] = SNOW;
		tc.HeightPass[4] = 256;

		break;
	case TerrainCreator::Plain:
		noise = 2;
		heightVals = ds.CreateDiamondSquare(mapSize, mapSize - 1, noise, 0, 10, 3);

		tc.Colors.resize(5);
		tc.HeightPass.resize(5);

		tc.Colors[0] = WATER;
		tc.HeightPass[0] = 10;

		tc.Colors[1] = SAND;
		tc.HeightPass[1] = 10;

		tc.Colors[2] = GRASS;
		tc.HeightPass[2] = 100;

		tc.Colors[3] = ROCK;
		tc.HeightPass[3] = 100;

		tc.Colors[4] = SNOW;
		tc.HeightPass[4] = 100;

		break;
	case TerrainCreator::Mountain:
		noise = 7;
		heightVals = ds.CreateDiamondSquare(mapSize, mapSize - 1, noise, -11, 17, 2);

		tc.Colors.resize(5);
		tc.HeightPass.resize(5);

		tc.Colors[0] = WATER;
		tc.HeightPass[0] = 2;

		tc.Colors[1] = SAND;
		tc.HeightPass[1] = 15;

		tc.Colors[2] = GRASS;
		tc.HeightPass[2] = 30;

		tc.Colors[3] = ROCK;
		tc.HeightPass[3] = 60;

		tc.Colors[4] = SNOW;
		tc.HeightPass[4] = 256;

		break;
	case TerrainCreator::Random:
	default:
		noise = rand() % 7 + 13;
		int min = (rand() % 25) * -1;
		int max = min * -1 + (rand() % 15) + 1;
		int it = rand() % 3 + 1;
		int step = (mapSize - 1) / (rand() % 2 + 1) * 2;

		heightVals = ds.CreateDiamondSquare(mapSize, step, noise, min, max, it);

		tc.Colors.resize(5);
		tc.HeightPass.resize(5);

		tc.Colors[0] = WATER;
		tc.HeightPass[0] = 0;

		tc.Colors[1] = SAND;
		tc.HeightPass[1] = 30;

		tc.Colors[2] = GRASS;
		tc.HeightPass[2] = 75;

		tc.Colors[3] = ROCK;
		tc.HeightPass[3] = 100;

		tc.Colors[4] = SNOW;
		tc.HeightPass[4] = 256;
		break;
	}

	if (false)
	{
		sf::Image input;
		input.create(mapSize, mapSize);
		for (int y = 0; y < mapSize; y++)
		{
			for (int x = 0; x < mapSize; x++)
			{
				int index = x + y * mapSize;
				float c = heightVals[index];
			
				sf::Color col;
				col.r = c;
				col.g = c;
				col.b = c;
				col.a = 255;

				input.setPixel(x, y, col);
			}
		}

		input.saveToFile("assets/Heightmap/gen.png");
	}

	std::cout << "Creating Texture and define Regions\n";
	_createDataFromHeightmapSimple(heightVals, mapSize, tc);

	std::cout << "Finding Edges\n";
	m_regions.EdgeDetect();
}

const std::vector<Region::Line> & TerrainCreator::GetEdges() const
{
	return m_regions.GetEdges();
}

Region::Region_Type TerrainCreator::GetRegionAt(unsigned int x, unsigned int y) const
{
	return m_regions.At(x, y).t;
}

bool TerrainCreator::IsType(unsigned int x, unsigned int y, Region::Region_Type t) const
{
	return m_regions.IsType(x, y, t);
}

sf::Texture* TerrainCreator::GetTexture()
{
	return &m_texture;
}

sf::Color _blend(sf::Color a, sf::Color b, float f)
{
	sf::Vector3f _a, _b;
	_a.x = a.r;
	_a.y = a.g;
	_a.z = a.b;

	_b.x = b.r;
	_b.y = b.g;
	_b.z = b.b;

	sf::Vector3f res = _a + f *(_b - _a);
	sf::Color col(0,0,0);
	col.r = res.x;
	col.g = res.y;
	col.b = res.z;

	return col;
}

void TerrainCreator::_createDataFromHeightmapSimple(const std::vector<float>& input, unsigned int size, const TerrainColors & col)
{
	bool dbg = false;
	unsigned int X, Y;
	X = size;
	Y = size;
	unsigned int colors = col.Colors.size();
	sf::Image data;

	data.create(X, Y);

	m_regions.Create(X, Y);

	sf::Image water, sand, grass, rock, snow, blocked;
	water.loadFromFile("assets/Terrain/water.png");
	sand.loadFromFile("assets/Terrain/sand.png");
	grass.loadFromFile("assets/Terrain/grass.png");
	rock.loadFromFile("assets/Terrain/rock.png");
	snow.loadFromFile("assets/Terrain/snow.png");
	blocked.loadFromFile("assets/Terrain/blocked.png");

	Float3 lightDir(-1, -1, -1);
	lightDir = lightDir.normalize();
	Float3 lightColor(1, 1, 1);

	float scl = sqrt(size) * 0.2;
	float blendScale = 0.1;

	struct Triangle
	{
		Float3 a, b, c;
	};

	for (unsigned int y = 0; y < Y; y++)
	{
		for (unsigned int x = 0; x < X; x++)
		{
			Region::PixelRegion pr;
			float h = input[x + y * size];
			
			float d = 1.0;

			Triangle tris[4];
			float dots = 0;
			size_t nrOfTris = 0;

			// Define Triangles
			{
				Triangle triTemp;

				if (x > 0 && y > 0)
				{
					triTemp.a.x = x - 1;
					triTemp.a.y = input[x - 1 + y * size] * scl;
					triTemp.a.z = y;

					triTemp.b.x = x;
					triTemp.b.y = input[x + (y - 1) * size] * scl;
					triTemp.b.z = y - 1;

					triTemp.c.x = x;
					triTemp.c.y = h * scl;
					triTemp.c.z = y;
					tris[nrOfTris++] = triTemp;
				}
				if (x < X - 1 && y > 0)
				{
					triTemp.a.x = x;
					triTemp.a.y = input[x + (y - 1) * size] * scl;
					triTemp.a.z = y - 1;

					triTemp.b.x = x + 1;
					triTemp.b.y = input[x + 1 + y * size] * scl;
					triTemp.b.z = y;

					triTemp.c.x = x;
					triTemp.c.y = h * scl;
					triTemp.c.z = y;
					tris[nrOfTris++] = triTemp;
				}
				if (x < X - 1 && y < Y - 1)
				{
					triTemp.a.x = x + 1;
					triTemp.a.y = input[x + 1 + y * size] * scl;
					triTemp.a.z = y;

					triTemp.b.x = x;
					triTemp.b.y = input[x + (y + 1) * size] * scl;
					triTemp.b.z = y + 1;

					triTemp.c.x = x;
					triTemp.c.y = h * scl;
					triTemp.c.z = y;
					tris[nrOfTris++] = triTemp;
				}
				if (x > 0 && y < Y - 1)
				{
					triTemp.a.x = x;
					triTemp.a.y = input[x + (y + 1) * size] * scl;
					triTemp.a.z = y + 1;

					triTemp.b.x = x - 1;
					triTemp.b.y = input[x - 1 + y * size] * scl;
					triTemp.b.z = y;

					triTemp.c.x = x;
					triTemp.c.y = h * scl;
					triTemp.c.z = y;
					tris[nrOfTris++] = triTemp;
				}
			}
			// END Define Triangles
			Float3 up(0, 1, 0);
			Float3 nor;

			for (size_t i = 0; i < nrOfTris; i++)
			{
				Float3 ab = tris[i].a - tris[i].b;
				Float3 ac = tris[i].a - tris[i].c;

				nor = nor + ab.Cross(ac);
			}
			nor = nor / nrOfTris;
			nor = nor.normalize();
			pr.ang = up.Dot(nor);
			d = abs(lightDir.Dot(nor));

			sf::Color p = snow.getPixel(x % 256, y % 256);

			for (unsigned int c = 0; c < colors; c++)
			{
				if (h < col.HeightPass[c])
				{

					if (col.Colors[c] == WATER)
					{
						p = water.getPixel(x % 256, y % 256);
						pr.t = Region::Region_Type::Water;
						pr.ang = 1.0f;
					}
					else if (col.Colors[c] == SAND)
					{
						p = sand.getPixel(x % 256, y % 256);
						pr.t = Region::Region_Type::Land;
					}
					else if (col.Colors[c] == GRASS)
					{
						p = grass.getPixel(x % 256, y % 256);
						pr.t = Region::Region_Type::Land;

						if (c != 0)
						{
							sf::Color pB = sand.getPixel(x % 256, y % 256);
							float maxH = col.HeightPass[c];
							float minH = col.HeightPass[c - 1];
							maxH -= minH;
							maxH *= blendScale;
							if (h - minH < maxH)
							{
								p = _blend(pB, p, (h - minH) / maxH);
							}
						}
					}
					else if (col.Colors[c] == ROCK)
					{
						p = rock.getPixel(x % 256, y % 256);
						pr.t = Region::Region_Type::Land;

						if (c != 0)
						{
							sf::Color pB = grass.getPixel(x % 256, y % 256);
							float maxH = col.HeightPass[c];
							float minH = col.HeightPass[c - 1];
							maxH -= minH;
							maxH *= blendScale;
							if (h - minH < maxH)
							{
								p = _blend(pB, p, (h - minH) / maxH);
							}
						}
					}

					if (pr.ang < 0.5f && col.Colors[c] != WATER)
					{
						p = _blend(blocked.getPixel(x % 256, y % 256), p, pr.ang * 0.5f);
						pr.t = Region::Blocked;
					}

					if (col.Colors[c] != WATER)
					{
						Float3 fCol(p.r / 255.0, p.g / 255.0, p.b / 255.0);
						fCol = (fCol * lightColor * d + (fCol * 0.2)) * 255;
						fCol = fCol.Cap(0, 255);
						p.r = fCol.x;
						p.g = fCol.y;
						p.b = fCol.z;
					}

					data.setPixel(x, y, p);

					if (dbg)
					{
						if (pr.ang > 0.9)
							if (col.Colors[c] == WATER)
							{
								data.setPixel(x, y, sf::Color::Blue);
							}
							else
							{
								data.setPixel(x, y, sf::Color::Green);
							}
						else if (pr.ang > 0.5)
							data.setPixel(x, y, sf::Color::Green);
						else
							data.setPixel(x, y, sf::Color::Red);
					}

					m_regions.Set(x, y, pr);
					break;
				}

				if (c == colors - 1)
				{
					pr.t = Region::Region_Type::Land;
					if (c != 0)
					{
						sf::Color pB = rock.getPixel(x % 256, y % 256);
						float maxH = col.HeightPass[c];
						float minH = col.HeightPass[c - 1];
						maxH -= minH;
						maxH *= blendScale;
						if (h - minH < maxH)
						{
							p = _blend(pB, p, (h - minH) / maxH);
						}
					}

					if (pr.ang < 0.5f)
					{
						p = _blend(blocked.getPixel(x % 256, y % 256), p, pr.ang * 0.5f);
						pr.t = Region::Blocked;
					}

					Float3 fCol(p.r / 255.0, p.g / 255.0, p.b / 255.0);
					fCol = (fCol * lightColor * d + (fCol * 0.2)) * 255;
					fCol = fCol.Cap(0, 255);
					p.r = fCol.x;
					p.g = fCol.y;
					p.b = fCol.z;

					data.setPixel(x, y, p);

					if (dbg)
					{
						if (pr.ang > 0.9)
							data.setPixel(x, y, sf::Color::White);
						else if (pr.ang > 0.5)
							data.setPixel(x, y, sf::Color::Green);
						else
							data.setPixel(x, y, sf::Color::Red);
					}

					m_regions.Set(x, y, pr);
				}

			}
		}
	}

	m_texture.loadFromImage(data);
	//m_texture.setSmooth(true);
}
