#!/bin/bash

File="CMakeLists.txt"

add_exe=($(eval "find src/ -name '*.cpp'"))

echo "cmake_minimum_required (VERSION 2.6)" 		>  $File
echo "project (ProjectRts)" 						>> $File
echo "include_directories(submodule/SFML/include)" 	>> $File
echo "include_directories(inc)" 					>> $File
echo "add_subdirectory(submodule/SFML)" 			>> $File
echo "add_subdirectory(submodule/Box2D-cmake)" 		>> $File
echo "find_package(OpenGL)" 						>> $File
echo "" 											>> $File

echo "add_executable(projectRts" >> $File
echo "main.cpp"   >> $File
for i in ${add_exe[@]}
do
	echo "  $i" >> $File
done
echo "  )" 		>> $File
echo "" 		>> $File

echo "if(UNIX)"                  >> $File
echo "  target_link_libraries("  >> $File
echo "    projectRts"            >> $File
echo "    sfml-graphics"         >> $File
echo "    sfml-audio"            >> $File
echo "    sfml-network"          >> $File
echo "    sfml-system"           >> $File
echo "    sfml-window"           >> $File
echo "    pthread"               >> $File
echo "    \${OPENGL_gl_LIBRARY}" >> $File
echo "	tobanteGaming::Box2D"    >> $File
echo "    )"                     >> $File
echo "else()"                    >> $File
echo "  target_link_libraries("  >> $File
echo "    projectRts"            >> $File
echo "    sfml-graphics"         >> $File
echo "    sfml-audio"            >> $File
echo "    sfml-main"             >> $File
echo "    sfml-network"          >> $File
echo "    sfml-system"           >> $File
echo "    sfml-window"           >> $File
echo "    \${OPENGL_gl_LIBRARY}" >> $File
echo "	tobanteGaming::Box2D"    >> $File
echo "    )"                     >> $File
echo "endif()"                   >> $File