#include <iostream>
#include <time.h>
#include <cmath>
#include <SFML/Graphics.hpp>
#include "src/rendering/Renderer.h"
#include "src/util/Timer.h"
#include <imgui.h>
#include "src/game/entity/Entity.h"
#include "src/pathfinding/World.h"
#include "src/pathfinding/pathfinding.h"
#include "src/rendering/drawable/SfmlLine.h"
#include <Box2D/Box2D.h>
#include <stdio.h>
#include <algorithm>
#include "src/rendering/SFMLTextureLoader.h"
#include "src/Terrain_Generation/TerrainCreator.h"
//#include "src/util/Util.h"

enum END_TYPE
{
	Restart = 0,
	Quit = 1
};

const int WIDTH = 1280;
const int HEIGHT = 720;
//const int WORLD_SIZE = (unsigned int)(std::pow(2, 10) + 1);
//const int WORLD_SIZE = (unsigned int)(std::pow(2, 12) + 1);
const int WORLD_SIZE = (unsigned int)(std::pow(2, 13) + 1);

std::string boolToString(bool b)
{
	return b ? "true" : "false";
}

std::string float2ToString(const Float2& f)
{
	return std::to_string(f.x) + ", " + std::to_string(f.y);
}

END_TYPE run()
{
	std::cout << "World Size: " << WORLD_SIZE << std::endl;
	Camera cam(Float2(0,0), Float2(WIDTH / 4, HEIGHT / 4));
	cam.SetAsActive();
	END_TYPE et = Quit;
	
	SFML_Drawable unitBack;
	
	unitBack.SetColor(255, 255, 255, 128);

	unsigned seed = static_cast<unsigned>(time(0));
	seed = 1576483718;
	std::cout << "seed: " << seed << std::endl;

	srand(seed);

	const unsigned int DEPTH = 8U;

	Renderer::Initialize(Renderer::SFML, WIDTH, HEIGHT, false, true);
	Renderer* renderer = Renderer::GetInstance();

	Pathfinding::Initialize({ (float)WORLD_SIZE, (float)WORLD_SIZE }, { 0.0f, 0.0f }, DEPTH, 256U);
	Pathfinding* pf = Pathfinding::GetInstance();
	
	World::Init();
	World* world = World::GetInstance();
	world->CreateRandomWorld(WORLD_SIZE, TerrainCreator::Islands);

	std::vector<SFML_Line> terrainLines;

	{
		auto vec = world->GetEdges();
		terrainLines.resize(vec.size());
		int counter = 0;
		for (auto& l : vec)
		{
			terrainLines[counter].SetPosition(Convert::ConvertToSfmlVec(l.a),
				Convert::ConvertToSfmlVec(l.b));
			terrainLines[counter++].SetColor(255, 255, 255);
		}


	}

	SFML_Drawable background;
	background.SetSize(WORLD_SIZE, WORLD_SIZE);
	background.SetOutlineColor(0, 0, 0);
	background.SetOutlineThickness(2);
	background.SetOrigin(sf::Vector2f(0, 0));
	background.SetTexture(world->GetTerrainTexture());

	SFML_TextureLoader* tl = SFML_TextureLoader::GetInstance();
	tl->LoadFromFile("assets/Arrow.png", "dummy");
	tl->LoadFromFile("assets/Cursors/Sword.png", "Cursor_Sword");
	renderer->SetMouseCursor("Cursor_Sword");
	renderer->SetMouseVisible(true);

	Entity * unit = nullptr;
	std::vector<int> StaticTaggedForRemoval;
	std::vector<int> DynamicTaggedForRemoval;
	int unitIndex = -1;

	int NUMBER_OF_OBSTICLES = 1;
	std::vector<Entity> obsticles(NUMBER_OF_OBSTICLES);
	for (int i = 0; i < NUMBER_OF_OBSTICLES; i++)
	{
		obsticles[i].SetViewDistance(100.0f);
		obsticles[i].ActivateMovement(false);
		obsticles[i].SetStatic();
		obsticles[i].SetPosition(rand() % WORLD_SIZE, rand() % WORLD_SIZE);
		obsticles[i].SetSize(rand() % 100 + 30, rand() % 100 + 30);
		obsticles[i].SetColor(128, 128, 128);
		obsticles[i].SetOutlineColor(sf::Color::Black);
		obsticles[i].SetOutlineThickness(-3);
		obsticles[i].Update(0);
		while (pf->ExistCollisionFor(&obsticles[i]))
		{
			obsticles[i].SetPosition(rand() % WORLD_SIZE, rand() % WORLD_SIZE);
			obsticles[i].Update(0);
		}
		obsticles[i].PlaceInWorld();
		world->Update();
	}


	bool requestPath = false;
	
	unsigned int NR_OF_RANDOM_UNITS = 1;
	const unsigned int MAX_WAIT_TIME = 16;

	std::vector<Entity> randomUnits(NR_OF_RANDOM_UNITS);
	std::vector<std::vector<Float2>>	randomUnitsPath(NR_OF_RANDOM_UNITS);
	std::vector<Timer>					randomUnitsTimer(NR_OF_RANDOM_UNITS);
	std::vector<unsigned int>			randomUnitsForcePath(NR_OF_RANDOM_UNITS);
	
	for (unsigned int i = 0; i < NR_OF_RANDOM_UNITS; i++)
	{
		randomUnits[i].SetTexture(tl->GetTexture("dummy"));
		randomUnits[i].SetSpeed(rand() % 101 + 50);
		randomUnitsTimer[i].Start();
		randomUnitsForcePath[i] = rand() % MAX_WAIT_TIME + 1;
		float size = rand() % 21 + 5;
		randomUnits[i].SetSize(size, size);
		randomUnits[i].SetCircleHitbox(size * 0.5);
		randomUnits[i].SetPosition(rand() % WORLD_SIZE, rand() % WORLD_SIZE);
		randomUnits[i].Update(0);

		bool isOk = true;
		PathfindingObject::PATHING_TYPE pft = randomUnits[i].GetPathingType();

		Float2 pos = randomUnits[i].GetHitboxPosition();

		switch (pft)
		{
		case PathfindingObject::LAND:
			isOk = world->IsType(pos.x, pos.y, Region::Land);
			break;
		case PathfindingObject::LAND_ALL:
			isOk = !world->IsType(pos.x, pos.y, Region::Water);
			break;
		case PathfindingObject::WATER:
			isOk = world->IsType(pos.x, pos.y, Region::Water);
			break;
		case PathfindingObject::WATER_AND_LAND:
			isOk = !world->IsType(pos.x, pos.y, Region::Blocked);
			break;
		}

		while (pf->ExistCollisionFor(&randomUnits[i]) || !isOk)
		{
			size = rand() % 21 + 5;
			randomUnits[i].SetSize(size, size);
			randomUnits[i].SetCircleHitbox(size * 0.5);
			randomUnits[i].SetPosition(rand() % WORLD_SIZE, rand() % WORLD_SIZE);
			pos = randomUnits[i].GetHitboxPosition();
			switch (pft)
			{
			case PathfindingObject::LAND:
				isOk = world->IsType(pos.x, pos.y, Region::Land);
				break;
			case PathfindingObject::LAND_ALL:
				isOk = !world->IsType(pos.x, pos.y, Region::Water);
				break;
			case PathfindingObject::WATER:
				isOk = world->IsType(pos.x, pos.y, Region::Water);
				break;
			case PathfindingObject::WATER_AND_LAND:
				isOk = !world->IsType(pos.x, pos.y, Region::Blocked);
				break;
			}
			
			randomUnits[i].Update(0);
		}
		randomUnits[i].PlaceInWorld();
		world->Update();
	}

	unit = &randomUnits[0];
	cam.SetPosition(Convert::ConvertToFloat2(unit->GetPosition()));
	bool drawTree = false;
	bool drawNeighbourTiles = false;
	bool drawFullTree = true;
	bool pathingOk = true;
	bool drawLines = false;

	unsigned long int frameCounter = 0;
	double totalTime = 0.0;

	Timer timer;
	timer.Start();
	
	bool RightClickLastFrame = false;
	bool LeftClickLastFrame = false;
	world->StartPhysicsThread();
	const float CAM_SPEED = 50;
	while (renderer->IsRunning())
	{
		frameCounter++;
		float dt = timer.Restart();
		totalTime += dt;
		renderer->Clear();
		renderer->Update(dt);
		renderer->PollEvents();
		world->Update();

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			cam.Move(-CAM_SPEED * dt * cam.GetZoom(), .0f, true);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			cam.Move(CAM_SPEED * dt * cam.GetZoom(), .0f, true);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			cam.Move(.0f, CAM_SPEED * dt * cam.GetZoom(), true);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			cam.Move(.0f, -CAM_SPEED * dt * cam.GetZoom(), true);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Add))
			cam.MultiplyZoom(1.0f - dt * 0.7f);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Subtract))
			cam.MultiplyZoom(1.0f + dt * 0.7f);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Period))
			cam.Rotate(15 * dt);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Comma))
			cam.Rotate(-15 * dt);

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && unit)
			cam.SetPosition(Convert::ConvertToFloat2(unit->GetPosition()));
		
		if (!StaticTaggedForRemoval.empty())
		{
			for (int i = 0; i < StaticTaggedForRemoval.size(); i++)
			{
				if (obsticles[StaticTaggedForRemoval[i]].ReadyForDeletion())
				{
					obsticles.erase(obsticles.begin() + StaticTaggedForRemoval[i]);
					StaticTaggedForRemoval.erase(StaticTaggedForRemoval.begin() + i);
					NUMBER_OF_OBSTICLES--;
				}
			}
		}
		if (!DynamicTaggedForRemoval.empty())
		{
			for (int i = 0; i < DynamicTaggedForRemoval.size(); i++)
			{
				if (randomUnits[DynamicTaggedForRemoval[i]].ReadyForDeletion())
				{
					randomUnits.erase(randomUnits.begin() + DynamicTaggedForRemoval[i]);
					DynamicTaggedForRemoval.erase(DynamicTaggedForRemoval.begin() + i);
					NR_OF_RANDOM_UNITS--;
				}
			}
		}

		// Update and draw game stuff here =================== v
		background.DrawAsBackground();
		std::string label_draw_qt = "Draw QuadTree: " + boolToString(drawTree);
		std::string label_draw_neigh = "Draw Neighbour tiles: " + boolToString(drawNeighbourTiles);
		std::string label_draw_full_qt = "Draw Full Tree: " + boolToString(drawFullTree);
		
		ImGui::Begin("Menu", NULL, ImGuiWindowFlags_AlwaysAutoResize);
		{
			ImGui::Text("Quadtree settings");
			if (ImGui::Button(label_draw_qt.data()))
			{
				drawTree = !drawTree;
			}
			if (drawTree)
			{
				if (ImGui::Button(label_draw_neigh.data()))
				{
					drawNeighbourTiles = !drawNeighbourTiles;
				}

				if (ImGui::Button(label_draw_full_qt.data()))
				{
					drawFullTree = !drawFullTree;
					if (!drawFullTree)
						Pathfinding::DRAW_DEPTH = 0;
				}

				if (drawFullTree)
					Pathfinding::DRAW_DEPTH = DEPTH + 1;

				if (!drawFullTree)
				{
					ImGui::SliderInt("Draw Depth", (int*)&Pathfinding::DRAW_DEPTH, 0, DEPTH);
				}
			}
			ImGui::Text("Unit settings");
			std::string label_pathing_ok = "Enable Pathing: " + boolToString(pathingOk);
			std::string label_draw_lines = "Draw pathing lines: " + boolToString(drawLines);
			
			if (ImGui::Button(label_pathing_ok.data()))
			{
				pathingOk = !pathingOk;
			}
			if (ImGui::Button(label_draw_lines.data()))
			{
				drawLines = !drawLines;
			}

			ImGui::Text("Exit");
			if (ImGui::Button("Quit"))
			{
				et = Quit;
				renderer->Exit();
			}
			if (ImGui::Button("Restart"))
			{
				et = Restart;
				renderer->Exit();
			}
		}
		ImGui::End();
		
		Float2 p = renderer->GetMouseWorldPosition();
		
		bool RightClickThisFrame;
		if ((RightClickThisFrame = sf::Mouse::isButtonPressed(sf::Mouse::Right)) && unit && !unit->IsStatic())
		{
			unit->RequestSingleAgentPath(p);
			//unit->SetPath(pf->GetSingleAgentPath(unit, Convert::ConvertToFloat2(unit->GetPosition()), p));
			/*auto lol = pf->GetSingleAgentPath(unit, unit->GetHitboxPosition(), p);
			int c = 0;
			std::cout << "Has path: " << boolToString(!lol.empty()) << std::endl;
			for (auto p : lol)
				std::cout << "Path@" + std::to_string(c++) + ": " + float2ToString(p) + "\n";*/
		}

		if (unit && unitIndex != -1 && sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Delete))
		{
			unit->RemoveFromWorld();
			if (unit->IsStatic())
				StaticTaggedForRemoval.push_back(unitIndex);
			else
				DynamicTaggedForRemoval.push_back(unitIndex);
			unit->ActivateMovement(false);
			unitIndex = -1;
			unit = nullptr;
		}

		RightClickLastFrame = RightClickThisFrame;

		PathfindingObject mouse = PathfindingObject::CreateCircleHitbox(5, p);
		bool LeftClickThisFrame;
		if ((LeftClickThisFrame= sf::Mouse::isButtonPressed(sf::Mouse::Left)) && !LeftClickLastFrame)
		{
			auto col = pf->GetAllObjectsCollidingWith(&mouse);

			if (!col.empty())
			{
				unit = (Entity*)col[0];

				if (unit->IsStatic())
				{
					for (int i = 0; i < NUMBER_OF_OBSTICLES; i++)
					{
						if (unit == &obsticles[i])
						{
							unitIndex = i;
							break;
						}
					}
				}
				else
				{
					for (int i = 0; i < NR_OF_RANDOM_UNITS; i++)
					{
						if (unit == &randomUnits[i])
						{
							unitIndex = i;
							break;
						}
					}
				}

			}
			else
			{
				unit = nullptr;
				unitIndex = -1;
			}
		}
		LeftClickLastFrame = LeftClickThisFrame;

		std::string str = std::to_string(p.x) + "\n" + std::to_string(p.y);

		ImGui::BeginTooltip();
		ImGui::SetTooltip(str.data());
		ImGui::EndTooltip();

		for (unsigned int i = 0; i < NR_OF_RANDOM_UNITS; i++)
		{
			randomUnits[i].ActivateMovement(pathingOk);

			std::string info = "";
			info += "ID: " + std::to_string(i) + "\n";
			info += "Time: " + std::to_string(randomUnitsTimer[i].GetElapsedTime()) + "\n";
			info += "Force after: " + std::to_string(randomUnitsForcePath[i]) + "\n";
			info += "Position: " + float2ToString(randomUnits[i].GetHitboxPosition()) + "\n";
			info += "Size: " + float2ToString(randomUnits[i].GetHitboxSize()) + "\n";
			info += "Speed: " + std::to_string(randomUnits[i].GetSpeed()) + "\n";

			if (randomUnits[i].Intersects(mouse))
			{
				ImGui::BeginTooltip();
				ImGui::SetTooltip(info.data());
				ImGui::EndTooltip();
			}

			if (&randomUnits[i] == unit)
				continue;

			if (randomUnits[i].GetSingleAgentPath().empty() || randomUnitsTimer[i].GetElapsedTime() >= randomUnitsForcePath[i])
			{
				randomUnits[i].RequestSingleAgentPath(Float2(rand() % WORLD_SIZE + 1, rand() % WORLD_SIZE + 1));
				randomUnitsTimer[i].Restart();
				randomUnitsTimer[i].Start();
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape))
			renderer->Exit();
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			for (auto& l : terrainLines)
				l.Draw();
		}

		if (unit)
			if (drawNeighbourTiles)	pf->TestDrawNeighbours(unit);

		for (int i = 0; i < NUMBER_OF_OBSTICLES; i++)
		{
			obsticles[i].Update(dt);
			obsticles[i].Draw();
		}

		if (unit)
		{
			unitBack.SetSize(unit->GetSize() * 1.5f);
			unitBack.SetPosition(unit->GetPosition());
			unitBack.Draw();
		}

		for (unsigned int i = 0; i < NR_OF_RANDOM_UNITS; i++)
		{
			randomUnits[i].Update(dt);
			randomUnits[i].Draw();
			if (drawLines)
			{
				randomUnits[i].DrawPathingLines();
			}
		}

		if (drawTree) pf->Draw();

		// Update and draw game stuff here =================== ^
		
		renderer->Flush();

		if (totalTime >= 1.0)
		{
			renderer->SetWindowTitle("FPS: " + std::to_string(frameCounter) + " | Average frame time: " + std::to_string((totalTime / frameCounter) * 1000.0) + " ms");
			totalTime -= 1.0;
			frameCounter = 0;
		}
	}
	
	Renderer::Release();
	Pathfinding::Release();
	World::Release();

	return et;
}

int b2Test();

int main(int argc, char* argv[])
{
	B2_NOT_USED(argc);
	B2_NOT_USED(argv);

	//b2Test();

	END_TYPE et = Restart;
	while (et == Restart)
	{
		et = run();
	}
	
	return 0;
}

#include <stdio.h>

// This is a simple example of building and running a simulation
// using Box2D. Here we create a large ground box and a small dynamic
// box.
// There are no graphics for this example. Box2D is meant to be used
// with your rendering engine in your game engine.

float RadToDeg(float rad)
{
	return rad * (180.0f / b2_pi);
}
float DegToRad(float deg)
{
	return deg * (b2_pi / 180.0f);
}

#include <SFML/Graphics.hpp>
int b2Test()
{
	//B2_NOT_USED(argc);
	//B2_NOT_USED(argv);

	sf::Vector2f dSize(25.0f, 25.0f);
	sf::Vector2f gSize(800, 10);

	sf::RenderWindow wnd(sf::VideoMode(800, 600), "");

	// Define the gravity vector.
	b2Vec2 gravity(0.0f, 0.0f);

	// Construct a world object, which will hold and simulate the rigid bodies.
	b2World world(gravity);
	
	// Define the ground body.
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(400.0f, 600.0f);
	groundBodyDef.angle = DegToRad(0.0f);
	// Call the body factory which allocates memory for the ground body
	// from a pool and creates the ground box shape (also from a pool).
	// The body is also added to the world.
	b2Body* groundBody = world.CreateBody(&groundBodyDef);
	
	// Define the ground box shape.
	b2PolygonShape groundBox;
	
	// The extents are the half-widths of the box.
	groundBox.SetAsBox(gSize.x * 0.5f, gSize.y * 0.5f);

	// Add the ground fixture to the ground body.
	groundBody->CreateFixture(&groundBox, 0.0f);

	// Define the dynamic body. We set its position and call the body factory.
	b2BodyDef bodyDef;
	bodyDef.type = b2_dynamicBody;
	bodyDef.position.Set(400.0f, 0.0f);
	
	b2BodyDef def;
	def.type = b2_dynamicBody;
	def.position.Set(410.0f, 500.0f);

	b2Body* body = world.CreateBody(&bodyDef);
	b2Body* body2 = world.CreateBody(&def);

	// Define another box shape for our dynamic body.
	b2CircleShape shape;
	shape.m_radius = dSize.x * 0.5f;
	b2CircleShape shape2;
	shape2.m_radius = dSize.x * 0.5f;

	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(dSize.x * 0.5f, dSize.y * 0.5f);

	// Define the dynamic body fixture.
	b2FixtureDef fixtureDef;
	//fixtureDef.shape = &dynamicBox;
	fixtureDef.shape = &shape;

	// Set the box density to be non-zero, so it will be dynamic.
	fixtureDef.density = 1.0f;
	//fixtureDef.density = 30.0f;
	// Override the default friction.
	fixtureDef.friction = 0.3f;
	fixtureDef.restitution = 0.5f;
	b2FixtureDef fixtureDef2;
	fixtureDef2.shape = &shape;
	fixtureDef2.density = 1.0f;
	fixtureDef2.friction = 0.3f;
	fixtureDef2.restitution = 0.1f;

	// Add the shape to the body.
	body->CreateFixture(&fixtureDef);
	body2->CreateFixture(&fixtureDef2);

	// Prepare for simulation. Typically we use a time step of 1/60 of a
	// qsecond (60Hz) and 10 iterations. This provides a high quality simulation
	// in most game scenarios.
	
	int32 velocityIterations = 6;
	int32 positionIterations = 2;

	b2Vec2 position = body->GetPosition();
	float32 angle = body->GetAngle();
	b2Vec2 position2 = body2->GetPosition();
	float32 angle2 = body2->GetAngle();
	//sf::RectangleShape d;
	sf::CircleShape d;
	d.setPosition(position.x, position.y);
	d.setRotation(RadToDeg(angle));
	//d.setSize(dSize);
	d.setRadius(dSize.x * 0.5f);
	d.setFillColor(sf::Color::Green);
	d.setOrigin(dSize * 0.5f);
	sf::CircleShape d2;
	d2.setPosition(position2.x, position2.y);
	d2.setRotation(RadToDeg(angle2));
	//d.setSize(dSize);
	d2.setRadius(dSize.x * 0.5f);
	d2.setFillColor(sf::Color::Green);
	d2.setOrigin(dSize * 0.5f);

	sf::RectangleShape g;
	g.setPosition(groundBody->GetPosition().x, groundBody->GetPosition().y);
	g.setSize(gSize);
	g.setOrigin(gSize * 0.5f);
	g.setFillColor(sf::Color(128, 128, 128));
	g.setRotation(RadToDeg(groundBody->GetAngle()));

	//body->SetLinearVelocity({ 0, 500 });
	body->SetLinearDamping(5);
	body2->SetLinearDamping(5);
	float speed = 100;

	Timer t;
	t.Start();
	// This is our little game loop.
	while (wnd.isOpen())
	{
		sf::Event event;
		while (wnd.pollEvent(event))
		{
			//ImGui::SFML::ProcessEvent(event);

			if (event.type == sf::Event::Closed)
			{
				wnd.close();
			}
		}
		float dt = t.Restart();
		
		
		b2Vec2 lv = body->GetLinearVelocity();
		b2Vec2 low(-speed, -speed);
		b2Vec2 high(speed, speed);
		bool yes = false;

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			lv.x += speed;
			yes = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			lv.x -= speed;
			yes = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			lv.y -= speed;
			yes = true;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			lv.y += speed;
			yes = true;
		}

		if (yes)
		{
			lv.Normalize();
			lv *= speed;
			body->SetLinearVelocity(lv);
		}

		position = body->GetPosition();
		angle = body->GetAngle();
		position2 = body2->GetPosition();
		angle2 = body2->GetAngle();

		if (body->GetLinearVelocity().Length() < 5)
			body->SetLinearVelocity(b2Vec2(0,0));

		/*b2Vec2 point(400.0f, 500.0f);
		float speed = 30.0f;
		angle = body->GetAngle();
		b2Vec2 dir = (point - position);
		dir.Normalize();
		dir.x *= speed;
		dir.y *= speed;
		body->SetLinearVelocity(dir);

		point = b2Vec2(410, 0);

		dir = (point - position2);
		dir.Normalize();

		dir.x *= speed;
		dir.y *= speed;
		body2->SetLinearVelocity(dir);*/

		d.setPosition(position.x, position.y);
		d.setRotation(RadToDeg(angle));
		d2.setPosition(position2.x, position2.y);
		d2.setRotation(RadToDeg(angle2));

		wnd.clear();
		wnd.draw(g);
		wnd.draw(d);
		wnd.draw(d2);

		wnd.display();
		world.Step(dt, velocityIterations, positionIterations);
	}
	
	// When the world destructor is called, all bodies and joints are freed. This can
	// create orphaned pointers, so be careful about your world management.

	return 0;
}